//
//  RootPageViewController.swift
//  RideApp
//
//  Created by Velan Salis on 16/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class AppIntroViewController: UIPageViewController, UIPageViewControllerDataSource , UIPageViewControllerDelegate{

    private var pageControl = UIPageControl(frame: .zero)
    
    lazy var appIntroViewControllerList : [UIViewController] = {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let NativeAppIntroPage1 = storyBoard.instantiateViewController(withIdentifier: "AppIntroPage-1")
        let NativeAppIntroPage2 = storyBoard.instantiateViewController(withIdentifier: "AppIntroPage-2")
        let NativeAppIntroPage3 = storyBoard.instantiateViewController(withIdentifier: "AppIntroPage-3")
        return [NativeAppIntroPage1, NativeAppIntroPage2, NativeAppIntroPage3]
    }()
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = appIntroViewControllerList.firstIndex(of: pageContentViewController)!
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentViewControllerIndex : Int = appIntroViewControllerList.firstIndex(of: viewController) else { return nil }
        let nextViewControllerIndex : Int = currentViewControllerIndex + 1
        guard appIntroViewControllerList.count != nextViewControllerIndex else { return nil }
        guard appIntroViewControllerList.count > nextViewControllerIndex else { return nil }
        return appIntroViewControllerList[nextViewControllerIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentViewControllerIndex : Int = appIntroViewControllerList.firstIndex(of: viewController) else { return nil }
        let previousViewControllerIndex : Int = currentViewControllerIndex - 1
        guard previousViewControllerIndex >= 0 else { return nil }
        guard appIntroViewControllerList.count > previousViewControllerIndex else { return nil }
        return appIntroViewControllerList[previousViewControllerIndex]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        self.setupPageControl()
        
        if let firstView = appIntroViewControllerList.first {
            self.setViewControllers([firstView], direction: .forward, animated: true, completion: nil)
        }
    }

    private func setupPageControl() {
        pageControl.numberOfPages = appIntroViewControllerList.count
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.currentPageIndicatorTintColor = UIColor.orange
        pageControl.pageIndicatorTintColor = .gray
        let leading = NSLayoutConstraint(item: pageControl, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: pageControl, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: pageControl, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -20)
        view.insertSubview(pageControl, at: 0)
        view.bringSubviewToFront(pageControl)
        view.addConstraints([leading, trailing, bottom])
    }
    
}
