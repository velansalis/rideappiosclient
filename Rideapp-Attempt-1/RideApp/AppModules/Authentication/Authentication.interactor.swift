//
//  Authentication.interactor.swift
//  RideApp
//
//  Created by Velan Salis on 20/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import Foundation

class AuthenticationInteractor {
    
    private let BASE_URL = "http://localhost:3000/api"
    private let loginURL : URL?
    private let registerURL : URL?
    
    init() {
        loginURL = URL(string: "\(BASE_URL)/auth/login")!
        registerURL = URL(string: "\(BASE_URL)/auth/register")!
    }
        
    private func request(url: URL, method : String, body : [String : Any]) -> Data? {
        let httpSession = URLSession.shared
        var httpRequest = URLRequest(url: url)
        var httpResponseData = Data()
        httpRequest.httpMethod = method
        httpRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        httpRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        do {
            httpRequest.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
        
        let task = httpSession.dataTask(with: httpRequest as URLRequest) { data, response, error in
            guard error == nil else { print(error!.localizedDescription); return }
            guard data != nil else { print("no data was fetched"); return }
            httpResponseData = data!
            dispatchSemaphore.signal()
        }
        
        task.resume()
        _ = dispatchSemaphore.wait(timeout: DispatchTime.distantFuture)
        return httpResponseData
    }
    
    func loginUser(params : [String : Any]) -> LoginResponse {
        let method = "POST"
        let body = params
        let responseData = request(url: loginURL!, method: method, body: body)
        let jsonDecoder = JSONDecoder()
        let parsedJson : LoginResponse = try! jsonDecoder.decode(LoginResponse.self, from: responseData!)
        return parsedJson
    }


    func registerUser(params : [String : Any]) -> RegisterResponse {
        let method = "POST"
        let body = params
        let responseData = request(url: registerURL!, method: method, body: body)
        let jsonDecoder = JSONDecoder()
        let parsedJson : RegisterResponse = try! jsonDecoder.decode(RegisterResponse.self, from: responseData!)
        return parsedJson
    }


    
}
