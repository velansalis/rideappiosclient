//
//  ForgotPasswordViewController.swift
//  RideApp
//
//  Created by Velan Salis on 17/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()
    }
    
    func initializeComponents() {
        self.configureNavBar()
    }

    func configureNavBar() {
        // Configuring back button for navigation bar
        let navBar = self.navigationController?.navigationBar
        let navItem = self.navigationItem
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "arrow.left"), for: .normal)
        backButton.addTarget(self, action: #selector(backNavButtonPressed), for: .touchUpInside)
        
        navBar?.tintColor = .darkGray
        navBar?.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar?.isTranslucent = true
        navBar?.shadowImage = UIImage()
        navItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backNavButtonPressed() {
        performSegue(withIdentifier: "ForgotPasswordToLoginSegue", sender: self)
    }

}
