//
//  LoginViewController.swift
//  CustomElement
//
//  Created by Velan Salis on 14/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: NativeTextField!
    @IBOutlet weak var passwordTextField: NativePasswordTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeNavigationBar()
    }
    
    private func initializeNavigationBar() {
        let navBar = self.navigationController?.navigationBar
        navBar?.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar?.isTranslucent = true
        navBar?.shadowImage = UIImage()
    }
    
    @IBAction func loginButtonPressed(_ sender: NativeButton) {
        performSegue(withIdentifier: "LoginToOTPSegue", sender: self)
    }
    
}
