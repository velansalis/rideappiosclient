//
//  RegisterViewController.swift
//  CustomElement
//
//  Created by Velan Salis on 15/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: NativeTextField!
    @IBOutlet weak var mobileNumberTextField: NativeTextField!
    @IBOutlet weak var emailTextField: NativeTextField!
    @IBOutlet weak var passwordTextField: NativePasswordTextField!
    @IBOutlet weak var registerButton: NativeButton!
    
    var activeTextField : NativeTextField = NativeTextField()
    var keyboardOverlayPadding : CGFloat = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationBar()
        self.addNotificationHandlers()
        self.customizeTextFields()
        self.customizeButton()
    }
    
    private func customizeNavigationBar() {
        let navBar = self.navigationController?.navigationBar
        let navItem = self.navigationItem
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage.init(named: "app bar"), for: .normal)
        backButton.addTarget(self, action: #selector(backNavButtonPressed), for: .touchUpInside)
        
        navBar?.barTintColor = UIColor.init(red: 237/255, green: 126/255, blue: 43/255, alpha: 1)
        navBar?.layer.shadowOpacity = 0.8
        navBar?.layer.shadowColor = UIColor.gray.cgColor
        navBar?.layer.shadowOffset = CGSize(width: 0, height: 2)
        navItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc private func backNavButtonPressed() {
        self.performSegue(withIdentifier: "RegisterToLoginSegue", sender: self)
    }
    
    private func addNotificationHandlers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func customizeTextFields() {
        nameTextField.addTarget(self, action: #selector(textFieldGainedFocus), for: .editingDidBegin)
        mobileNumberTextField.addTarget(self, action: #selector(textFieldGainedFocus), for: .editingDidBegin)
        emailTextField.addTarget(self, action: #selector(textFieldGainedFocus), for: .editingDidBegin)
        passwordTextField.addTarget(self, action: #selector(textFieldGainedFocus), for: .editingDidBegin)
        
        nameTextField.addTarget(self, action: #selector(textFieldLostFocus), for: .editingDidEnd)
        mobileNumberTextField.addTarget(self, action: #selector(textFieldLostFocus), for: .editingDidEnd)
        emailTextField.addTarget(self, action: #selector(textFieldLostFocus), for: .editingDidEnd)
        passwordTextField.addTarget(self, action: #selector(textFieldLostFocus), for: .editingDidEnd)
        
        nameTextField.invalidTextFieldLabel.text = "Invalid Name"
        mobileNumberTextField.invalidTextFieldLabel.text = "Invalid Phone Number"
        emailTextField.invalidTextFieldLabel.text = "Invalid Email ID"
        passwordTextField.invalidTextFieldLabel.text = "Password should contain at least one lowercase, one uppercase and one digit"
    }
    
    private func customizeButton() {
        self.registerButton.backgroundColor = .lightGray
        self.registerButton.isEnabled = false
    }

    @IBAction func registerButtonPressed(_ sender: UIButton) {
        print("Registering")
    }
    
}

extension RegisterViewController {
    @objc func keyboardWillShow(notification : Notification) {
        print("notified")
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let activeTextFieldHeight = self.activeTextField.frame.height
            let activeTextFieldYPosition = self.activeTextField.frame.minY + activeTextFieldHeight
            let requiredHeightForKeyboard = keyboardSize.height - (view.frame.height - activeTextFieldYPosition) + keyboardOverlayPadding
            if requiredHeightForKeyboard <= 0 {
                 view.frame.origin = CGPoint(x: 0, y: 0)
            } else {
                view.frame.origin = CGPoint(x: 0, y: requiredHeightForKeyboard * -1)
//                navigationController!.setNavigationBarHidden(true, animated: true)
            }
        }
    }
    
    @objc func keyboardWillHide(notification : Notification) {
        rearrangeView()
    }
}

extension RegisterViewController {
    
    @objc func textFieldGainedFocus(_ textField: NativeTextField) {
        print(textField.tag)
        self.activeTextField = textField
    }
    
    @objc func textFieldLostFocus(_ textField : NativeTextField) {
        let validation = Validation()
        if !validation.validateName(name: textField.text!) {
            textField.showErrorMessage()
        } else {
            textField.hideErrorMessage()
        }
        enableRegisterButtonIfValid()
    }
    
    func enableRegisterButtonIfValid() {
        if nameTextField.isValid && emailTextField.isValid && mobileNumberTextField.isValid && passwordTextField.isValid {
            registerButton.isEnabled = true
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        rearrangeView()
    }
    
    private func rearrangeView() {
        let navBarHeight = navigationController!.navigationBar.frame.height
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        navigationController!.setNavigationBarHidden(false, animated: true)
        view.frame.origin = CGPoint(x: 0, y: navBarHeight + statusBarHeight)
    }
    
}
