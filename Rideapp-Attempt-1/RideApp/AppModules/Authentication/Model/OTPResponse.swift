//
//  OTPResponse.swift
//  RideApp
//
//  Created by Velan Salis on 20/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import Foundation

struct OTPResponse : Codable {
    struct Data : Codable {
        let token : String
    }
    
    let data : Data?
    
    let name : String?
    let message : String?
}
