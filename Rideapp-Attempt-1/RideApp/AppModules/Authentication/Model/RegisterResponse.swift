//
//  RegisterResponse.swift
//  RideApp
//
//  Created by Velan Salis on 20/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import Foundation

struct RegisterResponse : Codable {
    struct Data : Codable {
        let otpVerificationID : String
    }
    
    let data : Data?
    
    let name : String?
    let message : String?
}
