//
//  UserModel.swift
//  RideApp
//
//  Created by Velan Salis on 19/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import Foundation

class UserModel : Codable {
    
    var _id : String = ""
    var name : String = ""
    var email : String = ""
    var phone : String = ""
    var password : String = ""
    var avatar : String?
    var joinedTrips : [String]?
    var services : [String]?
    var status : String?
    var createdAt : String?
    var updatedAt : String?
    
    init(name : String, email : String, phone : String, password : String) {
        self.name = name
        self.email = email
        self.phone = phone
        self.password = password
    }
    
}
