//
//  NativeButton.swift
//  CustomElement
//
//  Created by Velan Salis on 30/05/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class NativeButton: UIButton {
    
    // NativeButton Values
    // Changing these values will change the style of all the subsidiary
    let firstColor: CGColor = UIColor(red: 0.929, green: 0.494, blue: 0.168, alpha: 1).cgColor
    let secondColor: CGColor = UIColor(red: 0.956, green: 0.63, blue:  0.39, alpha: 1).cgColor
    let cornerRadius : CGFloat = 20
    let borderWidth : CGFloat = 0
    let borderColor : CGColor = UIColor.black.cgColor
    let shadowColor : CGColor = UIColor.black.cgColor
    let shadowOpacity : Float = 0.5
    let shadowOffset : CGSize = .zero
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame : frame)
        self.initializeComponenets()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        self.initializeComponenets()
    }
    
    private func initializeComponenets() -> Void {
        self.applyBasicStyles()
        self.applyShadow()
        self.applyGradient()
    }
    
    private func applyBasicStyles() -> Void {
        self.backgroundColor = .red
        self.titleColor(for: .focused)
        self.layer.cornerRadius = self.cornerRadius
        self.layer.borderWidth = self.borderWidth
        self.layer.borderColor = self.borderColor
    }
    
    private func applyShadow() -> Void {
        self.layer.masksToBounds = false
        self.layer.shadowColor = self.shadowColor
        self.layer.shadowOpacity = self.shadowOpacity
        self.layer.shadowOffset = self.shadowOffset
    }
    
    private func applyGradient() -> Void {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor]
        layer.startPoint = CGPoint(x: 0.0, y: 0.5)
        layer.endPoint = CGPoint(x: 1.0, y: 1.0)
    }
    
}
