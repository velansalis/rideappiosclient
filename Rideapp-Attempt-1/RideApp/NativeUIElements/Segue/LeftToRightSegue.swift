//
//  LeftToRightSegue.swift
//  RideApp
//
//  Created by Velan Salis on 19/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class LeftToRightSegue: UIStoryboardSegue {
    
    override init(identifier: String?, source: UIViewController, destination: UIViewController) {
        super.init(identifier: identifier, source: source, destination: destination)
    }
    
}
