//
//  NativePasswordTextField.swift
//  CustomElement
//
//  Created by Velan Salis on 15/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class NativePasswordTextField: NativeTextField {
    
    let passwordRevealButton : UIButton = UIButton(type: .custom)
    let passwordRevealButtonColor : UIColor = .black

    // Becomes first responder to prevent clearing of PasswordTextField on edit.
    override var isSecureTextEntry: Bool {
        didSet {
            if isFirstResponder {
                super.becomeFirstResponder()
                if isSecureTextEntry, let text = self.text {
                    deleteBackward()
                    insertText(text)
                }
            }
        }
    }
    
    // Initializes the NativeTextField if created programatically
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializeComponents()
    }
    
    // Initializes the NativeTextField if created from InterafaceBuilder/StoryBoard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeComponents()
    }
    
    private func initializeComponents() {
        self.disableAutoFill()
        self.customizeTextField()
    }
    
    // Special Customizations for the passwordField
    private func customizeTextField() {
        self.passwordRevealButton.tintColor = UIColor.gray
        self.passwordRevealButton.setImage(UIImage.init(named: "eye"), for: .normal)
        self.passwordRevealButton.contentMode = .scaleAspectFill
        self.passwordRevealButton.tintColor = self.passwordRevealButtonColor
        self.passwordRevealButton.addTarget(self, action: #selector(self.changePasswordVisibility), for: .touchUpInside)
        
        self.rightViewMode = .always
        self.isSecureTextEntry = true
        self.rightView = self.passwordRevealButton
    }
    
    // Prevention of Strong Password Overlay
    private func disableAutoFill() {
        if #available(iOS 12, *) {
            textContentType = .oneTimeCode
        } else {
            textContentType = .init(rawValue: "")
        }
    }
    
    @objc func changePasswordVisibility() {
        self.isSecureTextEntry = !self.isSecureTextEntry
        if self.isSecureTextEntry {
            self.passwordRevealButton.setImage(UIImage.init(named: "eye"), for: .normal)
        } else {
            self.passwordRevealButton.setImage(UIImage.init(named: "eye"), for: .normal)
        }
    }
    
}
