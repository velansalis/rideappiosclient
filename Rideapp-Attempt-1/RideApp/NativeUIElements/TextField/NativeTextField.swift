//
//  NativeTextField.swift
//  CustomElement
//
//  Created by Velan Salis on 13/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

@IBDesignable
class NativeTextField: UITextField {
    
    private let floatingLabel : UILabel = UILabel()
    private let floatingLabelTextColor: UIColor = UIColor.gray
    private let floatingLabelFontSize : CGFloat = 12
    
    private var temporaryPlaceholder : String = ""
    private var placeholderAnimationStartOffset : CGAffineTransform = CGAffineTransform(translationX: 0, y: 20)
    private var placeholderAnimationEndOffset : CGAffineTransform = CGAffineTransform(translationX: 0, y: 0)
    
    var invalidTextFieldLabel : UILabel = UILabel()
    private var invalidTextFieldLabelColor : UIColor = .red
    private let invalidTextFieldLabelSize : CGFloat = 10
    private let invalidTextFieldLabelWidth : CGFloat = 280
    
    private let leftImageColor : UIColor = .black
    private let leftImageDimentions = CGSize(width: 25, height: 25)
    private let leftImageViewHeight : CGFloat = 35
    
    var isValid : Bool = false
    
    // This inpsectable adds the left image to the textfield.
    @IBInspectable
    var leftImage: UIImage? {
        didSet {
            if let image = leftImage {
                leftViewMode = .always
                self.customizeLeftImage(image)
            }
            else {
                leftViewMode = .never
            }
        }
    }
    
    // UIImage : Customizations for the leftImage for the TextField
    private func customizeLeftImage(_ image : UIImage) {
        self.placeholderAnimationStartOffset = CGAffineTransform(translationX: 35, y: 20)
        self.placeholderAnimationEndOffset = CGAffineTransform(translationX: 35, y: 0)
        
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0,y: (leftImageViewHeight / 2) - leftImageDimentions.width / 2),size: leftImageDimentions))
        imageView.tintColor = self.leftImageColor
        imageView.contentMode = .scaleAspectFit
        imageView.image = image

        let view = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: leftImageViewHeight, height: leftImageViewHeight)))
        view.clipsToBounds = true
        view.addSubview(imageView)

        leftView = view
        
    }
    
    // Initializes the NativeTextField if created programatically
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeComponenets()
    }
    
    // Initializes the NativeTextField if created from InterafaceBuilder/StoryBoard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeComponenets()
    }
    
    private func initializeComponenets() {
        self.customizeSelf()
        self.customizeFloatingLabel()
        self.customizeTextInvalidLabel()
    }
    
    // UITextField : Customizations for the TextField
    private func customizeSelf() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 0.0
        if self.placeholder == nil { return self.placeholder = "" }
        
        self.addTarget(self, action: #selector(self.liftPlaceholder), for: .editingDidBegin)
        self.addTarget(self, action: #selector(self.dropPlaceholder), for: .editingDidEnd)
    }
    
    @objc func liftPlaceholder() {
        self.addSubview(self.floatingLabel)
        self.temporaryPlaceholder = self.placeholder!
        self.floatingLabel.bottomAnchor.constraint(equalTo:self.topAnchor, constant: 3).isActive = true
        self.placeholder = ""
        if self.text!.count == 0 {
            self.floatingLabel.transform = self.placeholderAnimationStartOffset
            UIView.animate(withDuration: 0.2, animations: {
                self.floatingLabel.transform = self.placeholderAnimationEndOffset
                self.floatingLabel.alpha = 1
            })
        }
    }
    
    @objc func dropPlaceholder() {
        if self.text!.count == 0 {
            UIView.animate(withDuration: 0.09, animations: {
                self.floatingLabel.transform = self.placeholderAnimationStartOffset
                self.floatingLabel.alpha = 0
            })
        }
        self.placeholder = self.temporaryPlaceholder
    }
    
    // UILabel : Styling for the textfield floating label
    private func customizeFloatingLabel() {
        self.floatingLabel.textColor = self.floatingLabelTextColor
        self.floatingLabel.font = UIFont.systemFont(ofSize: self.floatingLabelFontSize)
        self.floatingLabel.text = self.placeholder
        self.floatingLabel.translatesAutoresizingMaskIntoConstraints = false
        self.floatingLabel.shadowColor = UIColor.white
        self.floatingLabel.clipsToBounds = true
    }
    
    private func customizeTextInvalidLabel() {
        self.invalidTextFieldLabel.textColor = self.invalidTextFieldLabelColor
        self.invalidTextFieldLabel.font = UIFont.systemFont(ofSize: self.invalidTextFieldLabelSize)
        self.invalidTextFieldLabel.alpha = 0
        self.invalidTextFieldLabel.translatesAutoresizingMaskIntoConstraints = false
        self.invalidTextFieldLabel.shadowColor = UIColor.white
        self.invalidTextFieldLabel.clipsToBounds = true
        self.addSubview(self.invalidTextFieldLabel)
        self.invalidTextFieldLabel.topAnchor.constraint(equalTo:self.bottomAnchor, constant: 1).isActive = true
        self.invalidTextFieldLabel.widthAnchor.constraint(equalToConstant: invalidTextFieldLabelWidth).isActive = true
        self.invalidTextFieldLabel.numberOfLines = 0
    }
    
    func showErrorMessage() {
        self.isValid = false
        self.invalidTextFieldLabel.transform = CGAffineTransform(translationX: 0, y: -5)
        UIView.animate(withDuration: 0.1) {
            self.invalidTextFieldLabel.transform = CGAffineTransform(translationX: 0, y: 0)
            self.invalidTextFieldLabel.alpha = 1
        }
    }
    
    func hideErrorMessage() {
        self.isValid = true
        UIView.animate(withDuration: 0.1) {
            self.invalidTextFieldLabel.transform = CGAffineTransform(translationX: 0, y: -5)
            self.invalidTextFieldLabel.alpha = 0
        }
    }
    
}
