//
//  AlertOverlay.swift
//  RideApp
//
//  Created by Velan Salis on 18/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class AlertOverlay {
    
    func showLoadingOverlay(_ view : UIViewController ,message : String, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = .gray
        loadingIndicator.startAnimating();
        alert.view.addSubview(loadingIndicator)
        view.present(alert, animated: true, completion: completion)
    }
    
    func dismissLoadingOverlay(_ view : UIViewController, completion: (() -> Void)? = nil) {
        view.dismiss(animated: false, completion: completion)
    }
    
    func showAlertOverlay(_ view : UIViewController, title : String, message : String, actionButtonText : String, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionButtonText, style: .default, handler: nil))
        view.present(alert, animated: true, completion: completion)
    }
    
}
