//
//  Validation.swift
//  RideApp
//
//  Created by Velan Salis on 17/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import Foundation

class Validation {
    
    public func validateName(name: String) -> Bool {
        let nameRegex = "^[A-Za-z ]{3,18}$"
        return validate(value: name, regex: nameRegex)
    }
    
    public func validaPhoneNumber(phoneNumber: String) -> Bool {
        let phoneNumberRegex = "^[6-9]\\d{9}$"
        return validate(value: phoneNumber, regex: phoneNumberRegex)
    }
    
    public func validateEmailId(emailID: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return validate(value: emailID, regex: emailRegEx)
    }
    
    public func validatePassword(password: String) -> Bool {
        let passRegEx = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})"
        return validate(value: password, regex: passRegEx)
    }
    
}

extension Validation {
    
    private func validate(value : String, regex : String) -> Bool {
        let trimmedString = value.trimmingCharacters(in: .whitespaces)
        let validator = NSPredicate(format: "SELF MATCHES %@", regex)
        let isValid = validator.evaluate(with: trimmedString)
        return isValid
    }
    
}
