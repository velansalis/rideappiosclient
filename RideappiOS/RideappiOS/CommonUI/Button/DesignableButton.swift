//
//  DesignableButton.swift
//  RideappiOS
//
//  Created by Velan Salis on 23/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableButton: UIButton {

    @IBInspectable
    var shadow : Bool = false {
        didSet {
            if shadow {
                self.addShadow(shadowType: .depth2)
            }
            else {
                self.removeShadow()
            }
        }
    }

}
