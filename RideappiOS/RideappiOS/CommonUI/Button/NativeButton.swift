//
//  NativeButton.swift
//  RideappiOS
//
//  Created by Velan Salis on 29/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class NativeButton: DesignableButton {
    
    // NativeButton Values
    // Changing these values will change the style of all the subsidiary
    let firstColor: CGColor = UIColor(red: 0.929, green: 0.494, blue: 0.168, alpha: 1).cgColor
    let secondColor: CGColor = UIColor(red: 0.956, green: 0.63, blue:  0.39, alpha: 1).cgColor
    let cornerRadius : CGFloat = 20
    let borderWidth : CGFloat = 0
    let borderColor : CGColor = UIColor.black.cgColor
    
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame : frame)
        self.initializeComponenets()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        self.initializeComponenets()
    }
    
    private func initializeComponenets() -> Void {
        self.applyBasicStyles()
        self.applyGradient()
    }
    
    private func applyBasicStyles() -> Void {
        self.backgroundColor = .red
        self.tintColor = .white
        self.addCornerRadius(cornerRadius)
        self.layer.borderWidth = self.borderWidth
        self.layer.borderColor = self.borderColor
    }
    
    private func applyGradient() -> Void {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor]
        layer.startPoint = CGPoint(x: 0.0, y: 0.5)
        layer.endPoint = CGPoint(x: 1.0, y: 1.0)
    }
    
}
