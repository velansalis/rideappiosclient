//
//  NativeRoundedButton.swift
//  RideappiOS
//
//  Created by Velan Salis on 07/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class NativeRoundedButton: DesignableButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupComponents()
    }

    private func setupComponents() {
        
        self.addCornerRadius(self.layer.frame.width / 2)
    }
    
}
