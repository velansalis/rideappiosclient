//
//  OvalCollectionViewCell.swift
//  RideappiOS
//
//  Created by Velan Salis on 18/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class OvalCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupScreen()
        // Initialization code
    }
    
    private func setupScreen() {
        self.titleLabel.textColor = .orange
        
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor
        
        // It's obvious to use height/2. However, 2.2 gives smoother edge to the view radius.
        self.containerView.addCornerRadius(self.frame.height / 2.2)
        
        self.containerView.layer.borderWidth = 2.0
    }
    
    func setupValues(title: String) {
        self.titleLabel.text = title
    }

}
