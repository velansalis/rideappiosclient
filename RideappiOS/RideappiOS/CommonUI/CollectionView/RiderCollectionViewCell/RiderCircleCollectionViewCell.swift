//
//  RiderCollectionViewCell.swift
//  RideappiOS
//
//  Created by Velan Salis on 21/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

protocol RiderCircleCollectionViewCellDelegate {
    func riderCircleCellcloseButtonPressed(index: Int) -> Void
}

class RiderCircleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var invitedRiderCloseButton: UIButton!
    
    var delegate: RiderCircleCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupComponenets(invitedRider: Rider, closeButtonTag : Int) {
        self.invitedRiderCloseButton.tag = closeButtonTag
    }
    
    func setDismissableState(state: Bool) -> Void {
        // If the cell is dismissable, the close button should be shown
        // So, If Dismissable? = true, ButtonHidden? = false
        self.invitedRiderCloseButton.isHidden = !state
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        let index = invitedRiderCloseButton.tag
        delegate?.riderCircleCellcloseButtonPressed(index: index)
    }

}
