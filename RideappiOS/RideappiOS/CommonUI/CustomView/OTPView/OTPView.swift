//
//  OTPView.swift
//  RideApp
//
//  Created by Velan Salis on 17/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

protocol OTPViewDelegate : class {
    func OTPDidFill(text : String) -> Void
}

class OTPView: UIView {

    @IBOutlet weak var OTPField1: NativeTextfield!
    @IBOutlet weak var OTPField2: NativeTextfield!
    @IBOutlet weak var OTPField3: NativeTextfield!
    @IBOutlet weak var OTPField4: NativeTextfield!
    
    var OTPValues : String = ""
    weak var delegate : OTPViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame : frame)
        initializeComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        initializeComponents()
    }

    private func initializeComponents() {
        self.loadViewFromNib()
        self.initializeOTPFields()
    }
    
    private func loadViewFromNib() {
        let bundle = Bundle.init(for: type(of: self))
        let nib = UINib(nibName: "OTPView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    private func initializeOTPFields() {
        OTPField1.addTarget(self, action: #selector(textFieldDidChangeSelection(_:)), for: .editingChanged)
        OTPField2.addTarget(self, action: #selector(textFieldDidChangeSelection(_:)), for: .editingChanged)
        OTPField3.addTarget(self, action: #selector(textFieldDidChangeSelection(_:)), for: .editingChanged)
        OTPField4.addTarget(self, action: #selector(textFieldDidChangeSelection(_:)), for: .editingChanged)
        OTPField1.becomeFirstResponder()
    }
    
    @objc func textFieldDidChangeSelection(_ textField: UITextField) {
        focusOTPFields(textField)
    }
    
    
    private func focusOTPFields(_ textField: UITextField) {
        
        OTPValues = OTPField1.text! + OTPField2.text! + OTPField3.text! + OTPField4.text!
 
        if OTPValues.count == 4 {
            OTPField4.becomeFirstResponder()
            self.delegate?.OTPDidFill(text : OTPValues)
        }
        
        // When the current textfield is filled more than one characters
        // The focus should move to the next textfield
        if textField.text!.count > 1 {
            let nextOTPField = getNextOTPFieldOnFocus()
            nextOTPField.becomeFirstResponder()
            let extraCharacter = textField.text?.popLast()
            nextOTPField.text = String(extraCharacter!)
        }
        
        // When the current textfield becomes empty
        // focus should move to the previous textfield
        if textField.text!.count == 0 {
            let previousOTPField = getNextOTPFieldOnFocus()
            previousOTPField.becomeFirstResponder()
        }
    }
    
    func getNextOTPFieldOnFocus() -> NativeTextfield {
        let length = OTPValues.count
        var nextOTPField = NativeTextfield()
        if length == 0 { nextOTPField = OTPField1 }
        else if length == 1 { nextOTPField = OTPField1 }
        else if length == 2 { nextOTPField = OTPField2 }
        else if length == 3 { nextOTPField = OTPField3 }
        else if length == 4 { nextOTPField = OTPField4 }
        return nextOTPField
    }
    
}
