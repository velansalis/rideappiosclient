//
//  DatePickerPopupViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 23/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

protocol DatePickerPopupDelegate {
    func dateSelected(date: String) -> Void
}

class DatePickerPopupViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var senderTextfieldNameLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var readableDateLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    private let formatter = DateFormatter()
    var dateFormat: String = "dd-MM-yyyy" // This value can be changed via delegate when the segue is activated for better control over output format
    var delegate: DatePickerPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupScreen()
        setCurrentDate()
    }
    
    private func setupScreen() {
        self.containerView.addShadow(shadowType: .depth2)
        self.datePicker.addTarget(self, action: #selector(datepickerValueChanged), for: .valueChanged)
    }
    
    private func setupNavigation() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    deinit {
        print("[deinit] Date Picker")
    }

}

extension DatePickerPopupViewController {
    
    private func setCurrentDate() {
        let date = Date()
        datePicker.setDate(date, animated: true)
        setDatePickerValueOnScreen(datePicker: datePicker)
    }
    
    @IBAction func dateSelected(_ sender: UIButton) {
        formatter.dateFormat = dateFormat
        let date = formatter.string(from: datePicker.date)
        delegate?.dateSelected(date: date)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dateSelectionCancelled(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func datepickerValueChanged(_ sender: UIDatePicker) {
       setDatePickerValueOnScreen(datePicker: sender)
    }
    
    private func setDatePickerValueOnScreen(datePicker: UIDatePicker) {
        let _formatter = DateFormatter()
        _formatter.dateFormat = "yyyy"
        yearLabel.text = _formatter.string(from: datePicker.date)
        
        _formatter.dateFormat = "E, MMM d"
        readableDateLabel.text = _formatter.string(from: datePicker.date)
    }
    
}
