//
//  MapViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 30/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    // Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapActionButton: UIButton!
    
    // Class specific variables
    let locationManager = CLLocationManager()
    let mapPerspectiveInMeters: Double = 50 * 1000 // Because 1 KM = 1000 M
    
    // Map view specific attributes populated via segues
    var source: Place?
    var destination: Place?
    var milestones: [Milestone]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMap()
    }
    
    deinit {
        print("[deinit] MapView")
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    private func setupMap() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled() {
            checkLocationAuthorization()
        } else {
            // Alert is shown
        }
    }
    
    // Delegate Method (CLLocationManagerDelegate)
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        setRegionOnMap(mapView: mapView, location: location.coordinate, perspectiveInMeters: mapPerspectiveInMeters)
    }
    
    // Delegate Method (CLLocationManagerDelegate)
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
    func checkLocationAuthorization() {
        let locationStatus = CLLocationManager.authorizationStatus()
        
        switch locationStatus {
            case .authorizedWhenInUse:
                mapView.showsUserLocation = true
                centerViewOnUserLocation()
                locationManager.startUpdatingLocation()
                break
            case .denied:
                break
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .restricted:
                break
            case .authorizedAlways:
                break
        }
    }
    
    private func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            setRegionOnMap(mapView: mapView, location: location, perspectiveInMeters: mapPerspectiveInMeters)
        }
    }
    
    // This function takes in a map and sets its regions based on the location cordinate.
    private func setRegionOnMap(
        mapView: MKMapView,
        location: CLLocationCoordinate2D,
        perspectiveInMeters: Double
    ) {
        let region = MKCoordinateRegion.init(center: location, latitudinalMeters: perspectiveInMeters, longitudinalMeters: perspectiveInMeters)
        mapView.setRegion(region, animated: true)
    }
    
    
}
