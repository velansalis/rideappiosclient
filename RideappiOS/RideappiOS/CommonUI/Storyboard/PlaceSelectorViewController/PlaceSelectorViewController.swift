//
//  DestinationSelectorViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 06/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

protocol PlaceSelectorDelegate {
    func placeSelected(_ selectedPlace: Place) -> Void
}

class PlaceSelectorViewController: UIViewController {
    
    @IBOutlet weak var placeSearchTextfield: NativeTextfield!
    @IBOutlet weak var placeSelectorTableView: UITableView!
    
    var places : [Place]?
    var delegate: PlaceSelectorDelegate?
    
    var unwindSegueName : String = "unwindToPlaceSelected"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.populatePlaces()
        self.setupComponents()
        self.setupPlaceSelectorTableView()
    }
    
    private func setupComponents() {
        placeSearchTextfield.delegate = self
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func populatePlaces() {
        places = [
            Place(name: "Rohtang Pass", city: "Manali", pinCode: 574110, distance: 300, latlong: [32.3965, 77.2565]),
            Place(name: "Old Goa", city: "Goa", pinCode: 574110, distance: 300, latlong: [15.5014, 73.9314]),
            Place(name: "Hassan", city: "Hassan", pinCode: 574110, distance: 300, latlong: [13.0300, 76.0989]),
        ]
    }
    
    deinit {
        print("[deinit] Place Selector")
    }

}

extension PlaceSelectorViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Ended")
    }
}

extension PlaceSelectorViewController: UITableViewDataSource, UITableViewDelegate {
    
    func setupPlaceSelectorTableView() {
        let nib = UINib(nibName: "PlaceSelectorTableViewCell", bundle: nil)
        placeSelectorTableView.register(nib, forCellReuseIdentifier: "PlaceSelectorTableViewCell")
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var placesCount: Int = 0
        if let count = places?.count {
            placesCount = count
        }
        return placesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let place = places![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceSelectorTableViewCell") as! PlaceSelectorTableViewCell
        cell.setupValues(place: place)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedPlace = places![indexPath.row]
        delegate?.placeSelected(selectedPlace)
        dismiss(animated: true, completion: nil)
    }
    
}
