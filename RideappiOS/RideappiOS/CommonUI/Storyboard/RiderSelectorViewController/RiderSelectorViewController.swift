//
//  RiderSelectorViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 07/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

protocol RiderSelectorDelegate {
    func riderSelected(riders: [Rider]) -> Void
}

// TODO: Use class to pass the data between two view controllers.
// Take care of retain cycles
class RiderSelectorViewController: UIViewController {

    @IBOutlet weak var riderSelectorTableView: UITableView!
    let riderSelectorTableViewCellName: String = "RiderSelectorTableViewCell"
    
    var riders: [Rider] = []
    var selectedRiders: [Rider] = []
    var delegate: RiderSelectorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupComponents()
        populateRiderArray()
        setupRiderSelectorTableView()
    }
    
    private func setupNavigation() {
        self.title = "Invite People"
        self.navigationController?.setNativeNavigationBar(backButton: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func setupComponents() {
        self.riderSelectorTableView.delegate = self
        self.riderSelectorTableView.dataSource = self
    }
    
    private func populateRiderArray() {
        riders = [
            Rider(id: 1, name: "John Doe"),
            Rider(id: 2, name: "Jane Doe"),
            Rider(id: 3, name: "Doe Bro"),
            Rider(id: 4, name: "Bro Dro"),
        ]
    }
    
    override func willMove(toParent parent: UIViewController?) {
        if parent == nil {
            delegate?.riderSelected(riders: selectedRiders)
        }
    }
    
    deinit {
        print("[deinit] Rider Selector")
    }
    
}


extension RiderSelectorViewController: UITableViewDelegate, UITableViewDataSource {
    
    private func setupRiderSelectorTableView() {
        let nib = UINib(nibName: riderSelectorTableViewCellName, bundle: nil)
        riderSelectorTableView.register(nib, forCellReuseIdentifier: riderSelectorTableViewCellName)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return riders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rider = riders[indexPath.row]
        let cell = riderSelectorTableView.dequeueReusableCell(withIdentifier: riderSelectorTableViewCellName, for: indexPath) as! RiderSelectorTableViewCell
        
        var selected = false
        
        if isRiderPresentInArray(array: selectedRiders, rider: rider) { selected = true }
        
        cell.setupValues(rider: rider, selected: selected)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedRider: Rider = riders[indexPath.row]
        let selectedRiderCell: RiderSelectorTableViewCell = (tableView.cellForRow(at: indexPath) as! RiderSelectorTableViewCell)
        
        if isRiderPresentInArray(array: selectedRiders, rider: selectedRider) {
            deselectRider(rider: selectedRider, selectedRiderCell: selectedRiderCell)
        } else {
            selectRider(rider: selectedRider, selectedRiderCell: selectedRiderCell)
        }
        
    }
    
    private func selectRider(rider: Rider, selectedRiderCell: RiderSelectorTableViewCell) {
        selectedRiders.append(rider)
        selectedRiderCell.check()
    }
    
    private func deselectRider(rider: Rider, selectedRiderCell: RiderSelectorTableViewCell) {
        selectedRiders.removeAll(where: {$0.id == rider.id})
        selectedRiderCell.uncheck()
    }
    
    private func isRiderPresentInArray(array: [Rider], rider: Rider) -> Bool {
        var isPresent : Bool = false
        if array.contains(where: {$0.id == rider.id}) {
            isPresent = true
        }
        return isPresent
    }
    
}
