//
//  SuccessScreenViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 29/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

protocol SuccessScreenDelegate {
    func doneButtonPressed() -> Void
}

class SuccessScreenViewController: UIViewController {

    @IBOutlet weak var successTitle: UILabel!
    @IBOutlet weak var successDescription: UILabel!
    
    var successMessageTitle: String?
    var successMessageDescription: String?
    var delegate: SuccessScreenDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupScreen()
    }
    
    private func setupNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func setupScreen() {
        self.successTitle.text = successMessageTitle
        self.successDescription.text = successMessageDescription
    }
    
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        self.delegate?.doneButtonPressed()
    }
    
}
