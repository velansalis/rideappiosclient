//
//  TimePickerPopupViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 23/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

protocol TimePickerPopupDelegate {
    func timeSelected(time: String) -> Void
}

class TimePickerPopupViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var minutesLabel: UILabel!
    @IBOutlet weak var amIndicatorLabel: UILabel!
    @IBOutlet weak var pmIndicatorLabel: UILabel!
    @IBOutlet weak var timePicker: UIDatePicker!
    
    let formatter = DateFormatter()
    var timeFormat: String = "hh:mm a"
    var delegate: TimePickerPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupScreen()
        setCurrentTime()
    }
    
    private func setupNavigation() {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func setupScreen() {
        self.containerView.addShadow(shadowType: .depth2)
        self.timePicker.addTarget(self, action: #selector(timepickerValueChanged), for: .valueChanged)
    }
    
    deinit {
        print("[deinit] Time Picker")
    }
    
}

extension TimePickerPopupViewController {
    
    private func setCurrentTime() {
        let date = Date()
        timePicker.setDate(date, animated: true)
        setTimePickerValuesOnScreen(timePicker: timePicker)
    }
    
    @IBAction func timeSelected(_ sender: Any) {
        formatter.dateFormat = timeFormat
        let time = formatter.string(from: timePicker.date)
        delegate?.timeSelected(time: time)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func timeSelectionCancelled(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func timepickerValueChanged(_ sender: UIDatePicker) {
        setTimePickerValuesOnScreen(timePicker: sender)
    }
    
    private func setTimePickerValuesOnScreen(timePicker: UIDatePicker) {
        let _formatter = DateFormatter()
        _formatter.dateFormat = "hhmma"
        let formattedDate = Array(_formatter.string(from: timePicker.date))
        hoursLabel.text = "\(formattedDate[0] == "0" ? " " : formattedDate[0])\(formattedDate[1])"
        minutesLabel.text = "\(formattedDate[2])\(formattedDate[3])"
        
        let isAmOrPm = "\(formattedDate[4])\(formattedDate[5])"
        if isAmOrPm == "AM" {
            amIndicatorLabel.tintColor = .white
            pmIndicatorLabel.tintColor = .gray
        } else {
            amIndicatorLabel.tintColor = .gray
            pmIndicatorLabel.tintColor = .white
        }
    }
    
}
