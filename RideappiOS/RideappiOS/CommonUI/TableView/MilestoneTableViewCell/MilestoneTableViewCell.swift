//
//  MilestoneTableViewCell.swift
//  RideappiOS
//
//  Created by Velan Salis on 13/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

protocol MilestoneTableViewCellDelegate {
    func closeButtonPressed(indexPath: Int) -> Void
    func textfieldTouched(_ textField: UITextField, parent: MilestoneTableViewCell) -> Void
}

class MilestoneTableViewCell: UITableViewCell {
    
    @IBOutlet weak var milestoneCloseButton: UIButton!
    @IBOutlet weak var milestoneTitleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var sourceTextfield: NativeTextfield!
    @IBOutlet weak var currentLocationView: UIView!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var destinationTextfield: UITextField!
    
    var milestoneCellDelegate : MilestoneTableViewCellDelegate?
    
    @IBAction func closeButtonPressedInNib(_ sender: UIButton) {
        self.milestoneCellDelegate?.closeButtonPressed(indexPath: self.milestoneCloseButton.tag)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.sourceTextfield.delegate = self
        self.destinationTextfield.delegate = self
        
        setupComponenets()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
    }
    
    private func setupComponenets() {
        self.containerView.addCornerRadius(10)
        self.containerView.addShadow(shadowType: .depth1)
        
        self.currentLocationView.addCornerRadius(10)
        self.currentLocationView.addShadow(shadowType: .depth1)
        
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
    }
    
    private func getCurrentLocation() {
        currentLocationButton.titleLabel?.text = "Udupi"
    }
    
    private func addGradient() {
        let gradient = CAGradientLayer()
        gradient.frame = containerView.bounds
        gradient.locations = [0.0, 0.25]
        gradient.colors = [UIColor.init(red: 237/255, green: 126/266, blue: 43/255, alpha: 0.2).cgColor, UIColor.white.cgColor]
        containerView.clipsToBounds = true
        containerView.layer.insertSublayer(gradient, at: 0)
    }
    
    func setupValues(milestone: Milestone, indexPath: Int) {
        self.milestoneTitleLabel.text = "Milestone \(indexPath + 1)"
        self.milestoneCloseButton.tag = indexPath
        self.tag = indexPath
    }
    
}

extension MilestoneTableViewCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.milestoneCellDelegate?.textfieldTouched(textField, parent: self)
    }
    
}
