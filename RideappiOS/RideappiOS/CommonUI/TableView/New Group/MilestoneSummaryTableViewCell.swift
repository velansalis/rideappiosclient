//
//  MilestoneSummaryTableViewCell.swift
//  RideappiOS
//
//  Created by Velan Salis on 17/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class MilestoneSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var topTimelineStrand: UIView!
    @IBOutlet weak var bottomTimelineStrand: UIView!
    
    @IBOutlet weak var milestoneInfoContainerView: UIView!
    @IBOutlet weak var milestoneTitleLabel: UILabel!
    @IBOutlet weak var milestoneDistanceLabel: UILabel!
    @IBOutlet weak var milestoneTimeLabel: UILabel!
    @IBOutlet weak var milestoneSourceLabel: UILabel!
    @IBOutlet weak var milestoneDestinationLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupScreen()
    }
    
    private func setupScreen() {
        milestoneInfoContainerView.backgroundColor = .white
        milestoneInfoContainerView.addShadow(shadowType: .depth1)
        milestoneInfoContainerView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
        // Configure the view for the selected state
    }
    
    func setupValues(milestone: Milestone, index: Int) {
        milestoneTitleLabel.text = "Milestone \(index + 1)"
        milestoneSourceLabel.text = milestone.source?.city
        milestoneDestinationLabel.text = milestone.destination?.city
    }
    
}
