//
//  PlaceSelectorTableViewCell.swift
//  RideappiOS
//
//  Created by Velan Salis on 22/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class PlaceSelectorTableViewCell: UITableViewCell {

    @IBOutlet weak var tripContainerView: UIView!
    @IBOutlet weak var tripDestinationCity: UILabel!
    @IBOutlet weak var tripDestinationName: UILabel!
    @IBOutlet weak var tripDestinationDistance: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupComponents()
    }
    
    private func setupComponents() {
        self.addCornerRadius(10)
        self.tripContainerView.addCornerRadius(10)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
    }
    
    func setupValues(place : Place) {
        self.tripDestinationName.text = "\(place.name), \(place.city)"
        self.tripDestinationCity.text = "\(place.city) - \(place.pinCode)"
        self.tripDestinationDistance.text = "\(place.distance) KM"
    }
    
}
