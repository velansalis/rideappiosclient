//
//  PlaceThumbnailTableViewCell.swift
//  RideappiOS
//
//  Created by Velan Salis on 22/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class TripThumbnailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var tripContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupComponents()
    }
    
    private func setupComponents() {
        self.layer.cornerRadius = 10
        self.tripContainerView.layer.cornerRadius = 10
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
    }
    
    func setupValues(tripDestination : Place) {
        self.tripDestinationName.text = "\(tripDestination.name), \(tripDestination.city)"
        self.tripDestinationCity.text = "\(tripDestination.city) - \(tripDestination.pinCode)"
        self.tripDestinationDistance.text = "\(tripDestination.distance) KM"
    }
    
}
