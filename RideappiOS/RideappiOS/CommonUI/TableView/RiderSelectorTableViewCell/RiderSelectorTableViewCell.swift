//
//  RiderSelectorTableViewCell.swift
//  RideappiOS
//
//  Created by Velan Salis on 22/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class RiderSelectorTableViewCell: UITableViewCell {

    @IBOutlet weak var riderNameLabel: UILabel!
    @IBOutlet weak var riderSelectedImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    private func setupComponents() {}
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
        self.backgroundColor = .clear
    }
    
    func setupValues(rider: Rider, selected: Bool) {
        self.riderNameLabel.text = rider.name
        if selected { check() }
        else { uncheck() }
    }
    
    func check() {
        self.riderSelectedImage.image = UIImage(named: "checkmark-green")
    }
    
    func uncheck() {
        self.riderSelectedImage.image = UIImage(named: "checkmark-transparent")
    }
    
}
