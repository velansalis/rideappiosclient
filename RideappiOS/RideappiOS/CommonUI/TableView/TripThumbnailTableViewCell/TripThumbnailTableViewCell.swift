//
//  TripThumbnailTableViewCell.swift
//  RideappiOS
//
//  Created by Velan Salis on 22/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class TripThumbnailTableViewCell: UITableViewCell {

    @IBOutlet weak var cellContainerView: UIView!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardTitle: UILabel!
    
    let cellCornerRadius : CGFloat = 10
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupComponents()
    }
    
    private func setupComponents() {
        self.addCornerRadius(cellCornerRadius)
        
        self.cardImage.layer.masksToBounds = true
        self.cardImage.addCornerRadius(cellCornerRadius)
        self.cardImage.alpha = 0.6
        
        self.cellContainerView.backgroundColor = .black
        self.cellContainerView.addCornerRadius(cellCornerRadius)
        self.cellContainerView.addShadow(shadowType: .depth3)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
    }
    
    func setupValues(trip : Trip) {
        cardTitle.text = trip.name
        cardImage.image = trip.image
    }
    
}
