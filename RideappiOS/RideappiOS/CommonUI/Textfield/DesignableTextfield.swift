//
//  NativeDesignableTextfield.swift
//  RideappiOS
//
//  Created by Velan Salis on 05/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

protocol DesignableTextfieldDelegate : class {
    func rightViewTouchedInside() -> Void
}

@IBDesignable
class DesignableTextfield: UITextField {

    private let leftImageColor : UIColor = .black
    private let leftImageDimentions = CGSize(width: 20, height: 20)
    private let leftImageViewHeight : CGFloat = 35
    
    var invalidTextFieldLabel : UILabel = UILabel()
    private var invalidTextFieldLabelColor : UIColor = .red
    private let invalidTextFieldLabelSize : CGFloat = 11
    private let invalidTextFieldLabelWidth : CGFloat = 280
    
    var isValid : Bool = false
    var textfieldDelegate : DesignableTextfieldDelegate?
    
    var placeholderAnimationStartOffset : CGAffineTransform = CGAffineTransform(translationX: 0, y: 20)
    var placeholderAnimationEndOffset : CGAffineTransform = CGAffineTransform(translationX: 0, y: 0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customizeTextInvalidLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        self.customizeTextInvalidLabel()
    }
    
    @IBInspectable
    var leftImage: UIImage? {
        didSet {
            if let image = leftImage {
                leftViewMode = .always
                self.setupLeftImage(image)
            }
            else {
                leftViewMode = .never
            }
        }
    }

    private func setupLeftImage(_ image : UIImage) {
        self.placeholderAnimationStartOffset = CGAffineTransform(translationX: 35, y: 20)
        self.placeholderAnimationEndOffset = CGAffineTransform(translationX: 35, y: 0)
        
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 10,y: (leftImageViewHeight / 2) - leftImageDimentions.width / 2),size: leftImageDimentions))
        imageView.tintColor = self.leftImageColor
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        
        let view = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: leftImageViewHeight, height: leftImageViewHeight)))
        view.clipsToBounds = true
        view.addSubview(imageView)
        
        leftView = view
    }
    
    @IBInspectable
    var rightImage: UIImage? {
        didSet {
            if let image = rightImage {
                rightViewMode = .always
                self.setupRightImage(image)
            } else {
                rightViewMode = .never
            }
        }
    }
    
    private func setupRightImage(_ image: UIImage) {
        let rightViewButton = UIButton(type: .custom)
        rightViewButton.frame = CGRect(x:0, y:0, width:30, height:30)
        rightViewButton.setImage(image, for: .normal)
        rightViewButton.addTarget(self, action: #selector(rightButtonPressed), for: .touchUpInside)
        self.rightViewMode = .always
        self.rightView = rightViewButton
    }
    
    @objc private func rightButtonPressed() {
        self.textfieldDelegate?.rightViewTouchedInside()
    }
    
}

extension DesignableTextfield {
    private func customizeTextInvalidLabel() {
        self.invalidTextFieldLabel.textColor = self.invalidTextFieldLabelColor
        self.invalidTextFieldLabel.font = UIFont.systemFont(ofSize: self.invalidTextFieldLabelSize)
        self.invalidTextFieldLabel.alpha = 0
        self.invalidTextFieldLabel.translatesAutoresizingMaskIntoConstraints = false
        self.invalidTextFieldLabel.shadowColor = UIColor.white
        self.invalidTextFieldLabel.clipsToBounds = true
        self.addSubview(self.invalidTextFieldLabel)
        
        self.invalidTextFieldLabel.topAnchor.constraint(equalTo:self.bottomAnchor, constant: 1).isActive = true
        self.invalidTextFieldLabel.widthAnchor.constraint(equalToConstant: invalidTextFieldLabelWidth).isActive = true
        self.invalidTextFieldLabel.numberOfLines = 0
    }
    
    func showErrorMessage(_ message : String) {
        self.invalidTextFieldLabel.text = message
        self.invalidTextFieldLabel.transform = CGAffineTransform(translationX: 0, y: -5)
        UIView.animate(withDuration: 0.3) {
            self.invalidTextFieldLabel.transform = CGAffineTransform(translationX: 0, y: 0)
            self.invalidTextFieldLabel.alpha = 1
        }
        self.isValid = false
    }
    
    func hideErrorMessage() {
        UIView.animate(withDuration: 0.1) {
            self.invalidTextFieldLabel.transform = CGAffineTransform(translationX: 0, y: -5)
            self.invalidTextFieldLabel.alpha = 0
        }
        self.isValid = true
    }
}
