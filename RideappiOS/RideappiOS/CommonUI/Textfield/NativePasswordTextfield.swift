//
//  NativePasswordTextField.swift
//  CustomElement
//
//  Created by Velan Salis on 15/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class NativePasswordTextfield: NativeTextfield, DesignableTextfieldDelegate {
    
    override var isSecureTextEntry: Bool {
        didSet {
            if isFirstResponder {
                super.becomeFirstResponder()
                if isSecureTextEntry, let text = self.text {
                    deleteBackward()
                    insertText(text)
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializeComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeComponents()
    }
    
    private func initializeComponents() {
        self.disableAutoFill()
        self.customizeTextField()
    }

    private func customizeTextField() {
        self.isSecureTextEntry = true
        self.textfieldDelegate = self
    }

    private func disableAutoFill() {
        if #available(iOS 12, *) {
            textContentType = .oneTimeCode
        } else {
            textContentType = .init(rawValue: "")
        }
    }

    func rightViewTouchedInside() {
        self.isSecureTextEntry = !self.isSecureTextEntry
    }
    
}
