//
//  NativeSearchTextfield.swift
//  RideappiOS
//
//  Created by Velan Salis on 04/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

@IBDesignable
class NativeSearchTextfield: DesignableTextfield {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        setupComponents()
    }
    
    private func setupComponents() {
        self.addShadow(shadowType: .depth2)
    }

}
