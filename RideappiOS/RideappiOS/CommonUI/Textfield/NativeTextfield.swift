//
//  NativeTextfield.swift
//  RideappiOS
//
//  Created by Velan Salis on 29/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

enum NativeTextfieldType {
    case text
    case place
    case date
    case time
}

class NativeTextfield: DesignableTextfield {
    
    private let floatingLabel : UILabel = UILabel()
    private let floatingLabelTextColor: UIColor = UIColor.gray
    private let floatingLabelFontSize : CGFloat = 12
    
    private var temporaryPlaceholder : String = ""
    
    var type: NativeTextfieldType = .text
    
    // Initializes the NativeTextField if created programatically
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializeComponenets()
    }
    
    // Initializes the NativeTextField if created from InterafaceBuilder/StoryBoard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeComponenets()
    }
    
    private func initializeComponenets() {
        self.customizeSelf()
        self.customizeFloatingLabel()
    }
    
    // UITextField : Customizations for the TextField
    private func customizeSelf() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 0.0
        if self.placeholder == nil { return self.placeholder = "" }
        
        self.addTarget(self, action: #selector(self.liftPlaceholder), for: .editingDidBegin)
        self.addTarget(self, action: #selector(self.dropPlaceholder), for: .editingDidEnd)
    }
    
    @objc func liftPlaceholder() {
        self.addSubview(self.floatingLabel)
        self.temporaryPlaceholder = self.placeholder!
        self.floatingLabel.bottomAnchor.constraint(equalTo:self.topAnchor, constant: 3).isActive = true
        self.placeholder = ""
        if self.text!.count == 0 {
            self.floatingLabel.transform = self.placeholderAnimationStartOffset
            UIView.animate(withDuration: 0.2, animations: {
                self.floatingLabel.transform = self.placeholderAnimationEndOffset
                self.floatingLabel.alpha = 1
            })
        }
    }
    
    @objc func dropPlaceholder() {
        if self.text!.count == 0 {
            UIView.animate(withDuration: 0.09, animations: {
                self.floatingLabel.transform = self.placeholderAnimationStartOffset
                self.floatingLabel.alpha = 0
            })
        }
        self.placeholder = self.temporaryPlaceholder
    }
    
    private func customizeFloatingLabel() {
        self.floatingLabel.textColor = self.floatingLabelTextColor
        self.floatingLabel.font = UIFont.systemFont(ofSize: self.floatingLabelFontSize)
        self.floatingLabel.text = self.placeholder
        self.floatingLabel.translatesAutoresizingMaskIntoConstraints = false
        self.floatingLabel.shadowColor = UIColor.white
        self.floatingLabel.clipsToBounds = true
    }
    
    
}

