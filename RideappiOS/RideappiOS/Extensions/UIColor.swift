//
//  UIColor.swift
//  RideappiOS
//
//  Created by Velan Salis on 25/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

enum NativeColors {
    case PrimaryWhite
    case SecondaryWhite
    case PrimaryOrange
    case SecondaryOrange
    case LightGray
    case MediumGray
    case DarkGray
}

extension UIColor {
    
    static func getColor(_ color: NativeColors) -> UIColor {
        var uiColor: UIColor?
        switch color {
            case .PrimaryWhite:
                uiColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
                break
            case .SecondaryWhite:
                uiColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
                break
            case .PrimaryOrange:
                uiColor = UIColor(red: 0.929, green: 0.494, blue: 0.168, alpha: 1)
                break
            case .SecondaryOrange:
                uiColor = UIColor(red: 0.956, green: 0.63, blue:  0.39, alpha: 1)
                break
            case .LightGray:
                uiColor = UIColor.lightGray
                break
            case .MediumGray:
                uiColor = UIColor.gray
                break
            case .DarkGray:
                uiColor = UIColor.darkGray
                break
        }
        return uiColor!
    }
    
    static func getGradientLayer(_ colors: [CGColor], bounds: CGRect) -> CALayer {
        let layer: CAGradientLayer = CAGradientLayer()
        layer.frame = bounds
        layer.startPoint = CGPoint(x: 0.0, y: 0.5)
        layer.endPoint = CGPoint(x: 0.5, y: 1.0)
        layer.colors = colors
        return layer
    }
    
}
