//
//  UIImage.swift
//  RideappiOS
//
//  Created by Velan Salis on 25/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

extension UIImage {
    
    static func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage!
    }
    
}
