//
//  UINavigationController.swift
//  RideappiOS
//
//  Created by Velan Salis on 25/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func setTransparentNavigationBar(backButton: Bool) {
        navigationBar.tintColor = .black
        
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        navigationBar.isTranslucent = true
        navigationBar.shadowImage = UIImage()
        setNavigationBarHidden(false, animated:true)
        
        if backButton {
            self.navigationBar.backIndicatorImage = UIImage(named: "app bar-black")
            self.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "app bar-black")
            let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationBar.topItem?.backBarButtonItem = backButton
        }
    }
    
    func setNativeNavigationBar(backButton: Bool) {
        let barTintColor = [
            UIColor.getColor(.PrimaryOrange).cgColor,
            UIColor.getColor(.SecondaryOrange).cgColor
        ]
        
        let gradient = CAGradientLayer()
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let bounds = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
        
        gradient.frame = bounds
        gradient.colors = barTintColor
        UINavigationBar.appearance().setBackgroundImage(UIImage.image(fromLayer: gradient), for: .default)
        
        navigationBar.tintColor = .white
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationBar.addShadow(shadowType: .depth3)
        
        if backButton {
            self.navigationBar.backIndicatorImage = UIImage(named: "app bar")
            self.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "app bar")
            let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationBar.topItem?.backBarButtonItem = backButton
        }
    }
    
}
