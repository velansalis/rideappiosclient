//
//  UIView.swift
//  RideappiOS
//
//  Created by Velan Salis on 05/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

// The deeper you chose, darker the shadow gets
enum ShadowType {
    case depth0
    case depth1
    case depth2
    case depth3
}

// Shadow Specific
extension UIView {

    func addShadow(shadowType: ShadowType) {
        
        var shadowColor : CGColor?
        var shadowOpacity : Float?
        var shadowOffset : CGSize?
        var shadowRadius : CGFloat?
        
        switch shadowType {
            case .depth0:
                shadowColor = UIColor.clear.cgColor
                shadowOpacity = 0.0
                shadowOffset = .zero
                shadowRadius = 0
                break
            case .depth1:
                shadowColor = UIColor.black.cgColor
                shadowOpacity = 0.2
                shadowOffset = CGSize(width: 0, height: 0)
                shadowRadius = 3
                break
            case .depth2:
                shadowColor = UIColor.black.cgColor
                shadowOpacity = 0.35
                shadowOffset = CGSize(width: 0, height: 0)
                shadowRadius = 5
                break
            case .depth3:
                shadowColor = UIColor.black.cgColor
                shadowOpacity = 0.5
                shadowOffset = CGSize(width: 0, height: 0)
                shadowRadius = 7
                break
        }
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor!
        self.layer.shadowOpacity = shadowOpacity!
        self.layer.shadowOffset = shadowOffset!
        self.layer.shadowRadius = shadowRadius!
        
    }
    
    func removeShadow() {
        self.layer.shadowOpacity = 0
    }
    
}

extension UIView {
    
    func addCornerRadius(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
    
}
