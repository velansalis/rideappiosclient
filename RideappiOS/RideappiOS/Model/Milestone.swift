//
//  Milestone.swift
//  RideappiOS
//
//  Created by Velan Salis on 09/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import Foundation

class Milestone {
    var source: Place?
    var destination: Place?
    
    init(source: Place?, destination: Place?) {
        self.source = source
        self.destination = destination
    }
}
