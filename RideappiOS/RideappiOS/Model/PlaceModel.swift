//
//  TripDestination.swift
//  RideappiOS
//
//  Created by Velan Salis on 06/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import Foundation

class PlaceModel {
    var name: String
    var city: String
    var pinCode: Int
    var distance: Int
    var latlong: Array<Float>
    
    init(name: String, city: String, pinCode: Int, distance: Int, latlong: Array<Float>) {
        self.name = name
        self.city = city
        self.pinCode = pinCode
        self.distance = distance
        self.latlong = latlong
    }
}
