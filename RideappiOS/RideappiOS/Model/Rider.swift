//
//  Rider.swift
//  RideappiOS
//
//  Created by Velan Salis on 07/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import Foundation

class Rider {
    var id: Int
    var name: String
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}
