//
//  Trip.swift
//  RideappiOS
//
//  Created by Velan Salis on 04/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class Trip {
    var name: String
    var startDate: String
    var startTime: String
    var endDate: String
    
    var source: Place
    var destination: Place
    var invitedRiders: [Rider]
    var milestones: [Milestone]
    
    var image: UIImage?
    
    init(
        image: UIImage?, name: String,
        startDate: String, startTime: String, endDate: String,
        source: Place, destination: Place,
        invitedRiders: [Rider], milestones: [Milestone]
    ){
        self.name = name
        self.startDate = startDate
        self.startTime = startTime
        self.endDate = endDate
        
        self.source = source
        self.destination = destination
        
        self.invitedRiders = invitedRiders
        self.milestones = milestones
        
        if let image = image { self.image = image }
    }
}
