//
//  LoginViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 29/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameField: NativeTextfield!
    @IBOutlet weak var passwordField: NativePasswordTextfield!
    let validation : ValidationHandler = ValidationHandler()
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        _ = validateUsernameField()
        _ = validatePassword()
        
        if usernameField.text == "admin@example.com" && passwordField.text == "admin" {
            performSegue(withIdentifier: "LoginToDashboard", sender: nil)
        }
    }
    
}

// This extension handles all the functions that are executed
// once the view prepares and loads.
extension LoginViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationController()
    }
    
    private func setupNavigationController() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}


// This extension handles the code that is related to the delegate and
// Its correlating functions
extension LoginViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField , reason: UITextField.DidEndEditingReason) {
        if textField.tag == 1 { _ = validateUsernameField() }
        if textField.tag == 2 { _ = validatePassword() }
    }
    
    private func validateUsernameField() -> Bool {
        var isValid : Bool = false
        if !validation.validateUsername(username: usernameField.text!) {
            usernameField.showErrorMessage("Invalid Email / Phone Number")
            isValid = false
        } else {
            usernameField.hideErrorMessage()
            isValid = true
        }
        return isValid
    }
    
    private func validatePassword() -> Bool {
        var isValid : Bool = false
        if passwordField.text == "" {
            passwordField.showErrorMessage("Password can't be blank")
            isValid = false
        } else {
            passwordField.hideErrorMessage()
            isValid = true
        }
        return isValid
    }
    
}

// This extension handles the code when segue is activated
extension LoginViewController {
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "LoginToOTP" {
            return validateUsernameField()
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier! == "LoginToOTP" {
            if let destination : OTPViewController = segue.destination as? OTPViewController {
                destination.username = usernameField.text!
                destination.segueOnSuccess = "OtpToResetPassword"
            }
        }
    }
}
