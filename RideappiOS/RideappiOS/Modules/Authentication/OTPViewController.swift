//
//  OTPViewController.swift
//  RideApp
//
//  Created by Velan Salis on 17/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController, OTPViewDelegate {
    
    @IBOutlet weak var OTPView: OTPView!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var resendCodeButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    
    var countryCode : String = "91"
    var username : String = ""
    var segueOnSuccess : String = ""
    
    private let alertOverlay : AlertHandler = AlertHandler()
    private var timer : Timer?
    private var totalTime : Int = 30
    
    @IBAction func resendCodeButtonPressed_(_ sender: UIButton) {
        self.startTimer()
    }
    
    internal func OTPDidFill(text: String) {
        self.alertOverlay.showLoadingOverlay(self, message: "Please Wait!", completion: nil)
        if text == "1234" { self.OTPIsValid() }
        else { self.OTPIsInvalid() }
    }
    
    private func OTPIsValid() {
        self.alertOverlay.dismissLoadingOverlay(self) {
            self.performSegue(withIdentifier: self.segueOnSuccess, sender: nil)
        }
    }
    
    private func OTPIsInvalid() {
        self.alertOverlay.dismissLoadingOverlay(self) {
            self.alertOverlay.showAlertOverlay(self, title: "Invalid OTP", message: "The OTP You have entered is invalid. Please check your inbox and try again.", actionButtonText : "Try Again")
        }
    }

    // Once the view loads
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupScreen()
        self.startTimer()
    }
    
    private func setupScreen() {
        self.OTPView.delegate = self
        remainingTimeLabel.text = String(self.totalTime)
        usernameLabel.text = "+\(countryCode) - \(username)"
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    private func startTimer() {
        self.remainingTimeLabel.text = String(self.totalTime)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        resendCodeButton.isEnabled = false
    }
    
    @objc private func updateTimer() {
        remainingTimeLabel.text = String(Int(remainingTimeLabel.text!)! - 1)
        
        if Int(remainingTimeLabel.text!)! == 0 {
            resendCodeButton.isEnabled = true
            timer?.invalidate()
            timer = nil
        }
    }
    
    // Before the view appears to the screen
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationBar()
        super.viewWillAppear(true)
    }
    
    private func setupNavigationBar() {
        self.navigationController?.setTransparentNavigationBar(backButton: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension OTPViewController {
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue.destination)
    }
}
