//
//  RegisterViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 30/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var nameTextfield: NativeTextfield!
    @IBOutlet weak var phoneTextfield: NativeTextfield!
    @IBOutlet weak var emailTextfield: NativeTextfield!
    @IBOutlet weak var passwordTextfield: NativePasswordTextfield!
    @IBOutlet weak var registerButton: NativeButton!
    
    let validation = ValidationHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupScreen()
    }
    
    private func setupScreen() {
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.nameTextfield.delegate = self
        self.phoneTextfield.delegate = self
        self.emailTextfield.delegate = self
        self.passwordTextfield.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationController()
        super.viewWillAppear(true)
    }
    
    private func setupNavigationController() {
        self.navigationItem.title = "Register"
        self.navigationController?.setNativeNavigationBar(backButton: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "RegisterToOTP", sender: nil)
    }
    
    
}

extension RegisterViewController {
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "RegisterToOTP" { return true }
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier! == "RegisterToOTP" {
            if let destination : OTPViewController = segue.destination as? OTPViewController {
                destination.username = phoneTextfield.text!
                destination.segueOnSuccess = "OTPToDashboard"
            }
        }
    }
    
}

extension RegisterViewController : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        let currentTextfield = textField as! NativeTextfield
        if textField.tag == 1 { validateName(currentTextfield) }
        if textField.tag == 2 { validatePhoneNumber(currentTextfield) }
        if textField.tag == 3 { validateEmailID(currentTextfield) }
        if textField.tag == 4 { validatePassword(currentTextfield) }
    }
    
    private func validateName(_ textfield: NativeTextfield) {
        if !validation.validateName(textfield.text!) {
            textfield.showErrorMessage("Invalid Name")
        } else {
            textfield.hideErrorMessage()
        }
    }
    
    private func validatePhoneNumber(_ textfield: NativeTextfield) {
        if !validation.validatePhoneNumber(textfield.text!) {
            textfield.showErrorMessage("Invalid Phone Number")
        } else {
            textfield.hideErrorMessage()
        }
    }
    
    private func validateEmailID(_ textfield: NativeTextfield) {
        if !validation.validateEmailId(textfield.text!) {
            textfield.showErrorMessage("Invalid Email ID")
        } else {
            textfield.hideErrorMessage()
        }
    }
    
    private func validatePassword(_ textfield: NativeTextfield) {
        if !validation.validatePassword(textfield.text!) {
            textfield.showErrorMessage("Password should contain at least capital letter, a small letter and a number")
        } else {
            textfield.hideErrorMessage()
        }
    }
    
}
