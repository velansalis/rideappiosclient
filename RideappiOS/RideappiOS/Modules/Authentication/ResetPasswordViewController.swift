//
//  ResetPasswordViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 02/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var resetPasswordTextfield1: NativePasswordTextfield!
    @IBOutlet weak var resetPasswordTextfield2: NativePasswordTextfield!
    
    var validation : ValidationHandler = ValidationHandler()
    
    @IBAction func resetPasswordButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "ResetPasswordToSuccess", sender: nil)
    }

}

extension ResetPasswordViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resetPasswordTextfield1.delegate = self
        resetPasswordTextfield2.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        self.navigationController?.setTransparentNavigationBar(backButton: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension ResetPasswordViewController : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        let currentTextfield = textField as! NativeTextfield
        if validatePassword(currentTextfield) { _ = comparePasswords() }
    }
    
    private func validatePassword(_ textfield: NativeTextfield) -> Bool {
        var isValid = false
        if !validation.validatePassword(textfield.text!) {
            textfield.showErrorMessage("Password should contain at least capital letter, a small letter and a number")
            isValid = false
        } else {
            textfield.hideErrorMessage()
            isValid = true
        }
        return isValid
    }
    
    private func comparePasswords() -> Bool {
        if resetPasswordTextfield1.text != "" && resetPasswordTextfield2.text != ""
            && (resetPasswordTextfield1.text != resetPasswordTextfield2.text) {
            resetPasswordTextfield1.showErrorMessage("Passwords don't match")
            resetPasswordTextfield2.showErrorMessage("Passwords don't match")
            return false
        } else {
            resetPasswordTextfield1.hideErrorMessage()
            resetPasswordTextfield2.hideErrorMessage()
            return true
        }
    }
    
}
