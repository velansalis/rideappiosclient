//
//  SuccessViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 02/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class SuccessPasswordViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension SuccessPasswordViewController {
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        self.navigationController?.setTransparentNavigationBar(backButton: false)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}
