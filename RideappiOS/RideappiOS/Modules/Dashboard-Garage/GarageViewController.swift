//
//  GarageViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 03/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class GarageViewController: UIViewController {

    var tabBarItemTitle = "Garage"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setTransparentNavigationBar(backButton: true)
    }

}
