//
//  CreateTripViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 05/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class CreateTripViewController: UIViewController {
    
    
    @IBOutlet weak var currentLocationContainerView: UIView!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var destinationTextfield: NativeTextfield!
    @IBOutlet weak var sourceTextfield: NativeTextfield!
    @IBOutlet weak var tripNameTextfield: NativeTextfield!
    @IBOutlet weak var startDateTextfield: NativeTextfield!
    @IBOutlet weak var endDateTextfield: NativeTextfield!
    @IBOutlet weak var startTimeTextfield: NativeTextfield!
    @IBOutlet weak var invitedUsersCollectionView: UICollectionView!
    @IBOutlet weak var inviteRidersPromptLabel: UILabel!
    
    @IBOutlet weak var milestoneTableView: UITableView!
    @IBOutlet weak var milestoneTableViewHeight: NSLayoutConstraint!
    
    // View Specific Variables
    let milestoneTableViewCellHeight : CGFloat = 300
    
    // Runtime values containers
    var selectedTextfield: UITextField?
    var editingMilestoneIndexPath: Int?
    var currentLocation: Place?
    
    // Models
    var source: Place?
    var destination: Place?
    var invitedRiders: [Rider] = []
    var milestones: [Milestone] = []
    
    // Segue Titles
    var placeSelectorSegue : String = "CreateTripToPlaceSelector"
    var riderSelectorSegue : String = "CreateTripToRiderSelector"
    var tripSummarySegue : String = "CreateTripToSummary"
    var datePickerPopupSegue: String = "ShowDatePickerPopup"
    var timePickerPopupSegue: String = "ShowTimePickerPopup"
    
    // Reusable Cell Titles
    @IBOutlet weak var invitedRiderCollectionView: UICollectionView!
    var invitedRiderCollectionViewCellName : String = "InvitedRiderCell"
    var milestoneTableViewCellName: String = "MilestoneCell"
    var riderCircleCollectionViewCellName: String = "RiderCircleCollectionViewCell"
    
    deinit {
        print("[deinit] Create Trip")
    }
}

// View Loading Workflow
extension CreateTripViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    private func setupComponents() {
        setupDatePickerTextfield()
        setupTimePickerTextfield()
        setupPlaceSelectorTextfield()
        setupInvitedRiderCollectionView()
        setupMilestoneTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupNavigationBar()
        setupScreen()
    }
    
    private func setupNavigationBar() {
        self.title = "Create a Trip"
        self.navigationController?.setNativeNavigationBar(backButton: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    private func setupScreen() {
        self.currentLocationContainerView.addShadow(shadowType: .depth1)
        self.currentLocationContainerView.addCornerRadius(5)
        
        let currentLocation = getCurrentLocation()
        self.setCurrentLocation(location: currentLocation)
    }
}

// Textfield Delegate
extension CreateTripViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        editingMilestoneIndexPath = nil
        
        let currentTextfield = textField as! NativeTextfield
        if currentTextfield.text == "" { currentTextfield.text = " " }
        selectedTextfield = currentTextfield
        
        if currentTextfield.type == .date { selectDate(currentTextfield) }
        else if currentTextfield.type == .place { selectPlace(currentTextfield) }
        else if currentTextfield.type == .time { selectTime(currentTextfield) }
        else if currentTextfield.type == .text {}
        
        selectedTextfield?.endEditing(true)
    }
    
}

// Date Selector Workflow
extension CreateTripViewController: DatePickerPopupDelegate {
    
    private func setupDatePickerTextfield() {
        startDateTextfield.delegate = self
        startDateTextfield.type = .date
        
        endDateTextfield.delegate = self
        endDateTextfield.type = .date
    }
    
    func dateSelected(date: String) {
        self.selectedTextfield?.text = date
    }
    
    private func selectDate(_ textField: NativeTextfield) {
        performSegue(withIdentifier: datePickerPopupSegue, sender: self)
    }
    
}

// Time Selector Workflow
extension CreateTripViewController: TimePickerPopupDelegate {
    
    private func setupTimePickerTextfield() {
        startTimeTextfield.delegate = self
        startTimeTextfield.type = .time
    }
    
    func timeSelected(time: String) {
        selectedTextfield!.text = time
    }
    
    private func selectTime(_ textfield: NativeTextfield) {
        performSegue(withIdentifier: timePickerPopupSegue, sender: self)
    }
    
}

// Milestones Workflow
extension CreateTripViewController: UITableViewDelegate, UITableViewDataSource, MilestoneTableViewCellDelegate {
    
    // Milestone View Utility Classes
    private func setupMilestoneTableView() {
        milestoneTableView.delegate = self
        milestoneTableView.dataSource = self
        let cellNib = UINib(nibName: "MilestoneTableViewCell", bundle: nil)
        milestoneTableView.register(cellNib, forCellReuseIdentifier: milestoneTableViewCellName)
        milestoneTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        milestoneTableViewHeight.constant = 5
    }
    
    // Milestone Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return milestones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let milestone = milestones[indexPath.row]
        let cell = milestoneTableView.dequeueReusableCell(withIdentifier: milestoneTableViewCellName, for: indexPath) as! MilestoneTableViewCell
        cell.setupValues(milestone: milestone, indexPath: indexPath.row)
        cell.milestoneCellDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return milestoneTableViewCellHeight
    }
    
    // Milestone View Delegate
    func textfieldTouched(_ textField: UITextField, parent: MilestoneTableViewCell) {
        textField.text = " "
        editingMilestoneIndexPath = parent.tag
        self.selectedTextfield = textField
        performSegue(withIdentifier: placeSelectorSegue, sender: self)
    }
    
    func closeButtonPressed(indexPath: Int) {
        removeMilestone(indexPath: indexPath)
        let cell = milestoneTableView.cellForRow(at: IndexPath(row: indexPath, section: 0)) as! MilestoneTableViewCell
        cell.removeFromSuperview()
        refreshMilestoneTableViewHeight()
    }
    
    private func refreshMilestoneTableViewHeight() {
        self.milestoneTableView.reloadData()
        self.milestoneTableViewHeight.constant = CGFloat(milestoneTableViewCellHeight) * CGFloat(self.milestones.count)
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    private func addMilestone(milestone: Milestone) {
        milestones.append(milestone)
    }
    
    private func removeMilestone(indexPath: Int) {
        milestones.remove(at: indexPath)
        let cell = milestoneTableView.cellForRow(at: IndexPath(row: indexPath, section: 0))
        cell?.removeFromSuperview()
    }
    
    // Add Milestone Button Pressed Action
    @IBAction func addMilestoneButtonPressed(_ sender: Any) {
        self.addMilestone(milestone: Milestone(source: nil, destination: nil))
        refreshMilestoneTableViewHeight()
    }
    
}

// Rider's Collection View Workflow
extension CreateTripViewController: UICollectionViewDelegate, UICollectionViewDataSource, RiderSelectorDelegate, RiderCircleCollectionViewCellDelegate {
    
    func setupInvitedRiderCollectionView() {
        invitedUsersCollectionView.delegate = self
        invitedUsersCollectionView.dataSource = self
        let nib = UINib(nibName: riderCircleCollectionViewCellName, bundle: nil)
        invitedRiderCollectionView.register(nib, forCellWithReuseIdentifier: riderCircleCollectionViewCellName)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return invitedRiders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: riderCircleCollectionViewCellName, for: indexPath) as! RiderCircleCollectionViewCell
        let invitedRider = invitedRiders[indexPath.row]
        cell.setupComponenets(invitedRider: invitedRider, closeButtonTag : indexPath.row)
        cell.delegate = self
        return cell
    }
    
    func riderSelected(riders: [Rider]) {
        invitedRiders = riders
        invitedUsersCollectionView.reloadData()
        
        if invitedRiders.count == 0 {
            self.inviteRidersPromptLabel.isHidden = false
        } else {
            self.inviteRidersPromptLabel.isHidden = true
        }
        
    }
    
    func riderCircleCellcloseButtonPressed(index: Int) {
        removeInvitedRider(index: index)
    }
    
    private func removeInvitedRider(index: Int) {
        invitedRiders.remove(at: index)
        if invitedRiders.count == 0 { self.inviteRidersPromptLabel.isHidden = false }
        invitedUsersCollectionView.reloadData()
    }
    
}

// Place Selector Workflow
extension CreateTripViewController: PlaceSelectorDelegate {
    
    private func setupPlaceSelectorTextfield() {
        self.sourceTextfield.delegate = self
        self.sourceTextfield.type = .place
        
        self.destinationTextfield.delegate = self
        self.destinationTextfield.type = .place
    }
    
    func placeSelected(_ selectedPlace: Place) {
        
        if let index = editingMilestoneIndexPath {
            if self.selectedTextfield?.tag == 0 {
                milestones[index].source = selectedPlace
            } else {
                milestones[index].destination = selectedPlace
            }
        } else {
            if self.selectedTextfield?.tag == 0 {
                source = selectedPlace
            } else {
                destination = selectedPlace
            }
        }
        
        self.selectedTextfield?.text = "\(selectedPlace.name), \(selectedPlace.city)"
    }
    
    private func setCurrentLocation(location: Place) {
        self.currentLocation = location
        self.currentLocationButton.setTitle(location.city, for: .normal)
    }
    
    private func getCurrentLocation() -> Place {
        let currentLocation = Place(name: "Udupi", city: "Udupi", pinCode: 574110, distance: 300, latlong: [200.12, 500.12])
        return currentLocation
    }
    
    @IBAction func currentLocationButtonClicked(_ sender: UIButton) {
        self.sourceTextfield.text = sender.titleLabel?.text
        self.source = currentLocation!
    }
    
    private func selectPlace(_ textField: NativeTextfield) {
        performSegue(withIdentifier: placeSelectorSegue, sender: self)
    }
    
}

// Navigation Workflow
extension CreateTripViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == placeSelectorSegue {
            if let destination: PlaceSelectorViewController = segue.destination as? PlaceSelectorViewController {
                destination.delegate = self
            }
        }
        
        if segue.identifier == riderSelectorSegue {
            if let destination: RiderSelectorViewController = segue.destination as? RiderSelectorViewController {
                destination.selectedRiders = invitedRiders
                destination.delegate = self
            }
        }
        
        if segue.identifier == datePickerPopupSegue {
            if let destination: DatePickerPopupViewController = segue.destination as? DatePickerPopupViewController {
                destination.delegate = self
            }
        }
        
        if segue.identifier == timePickerPopupSegue {
            if let destination: TimePickerPopupViewController = segue.destination as? TimePickerPopupViewController {
                destination.delegate = self
            }
        }
        
        if segue.identifier == tripSummarySegue {
            let name = tripNameTextfield.text
            let startDate = startDateTextfield.text
            let startTime = startTimeTextfield.text
            let endDate = endDateTextfield.text
            
            let trip = Trip(
                image: nil, name: name!, startDate: startDate!, startTime: startTime!, endDate: endDate!,
                source: source!, destination: destination!, invitedRiders: invitedRiders, milestones: milestones
            )

            if let destination : TripSummaryViewController = segue.destination as? TripSummaryViewController {
                destination.tripSummaryType = .notCreated
                destination.trip = trip
            }
            
        }
        
    }
    
}
