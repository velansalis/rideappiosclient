//
//  TripSummaryViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 09/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

// TripSummaryType
// Trip summary can be invoked in two occasions. One during the trip creation and one after the trip creation.
// So the two cases are specified in enum which will be populated by the parent segue. By specifying which type of trip
// summary you need, you get tailored trip summary that configurs itself based on its type.
enum TripSummaryType {
    case created
    case notCreated
}

class TripSummaryViewController: UIViewController {

    @IBOutlet weak var tripLocationMapView: MKMapView!
    @IBOutlet weak var tripInformationContainerView: UIView!
    @IBOutlet weak var tripNameLabel: UILabel!
    @IBOutlet weak var tripStartDateLabel: UILabel!
    @IBOutlet weak var tripStartTimeLabel: UILabel!
    @IBOutlet weak var tripSourceLabel: UILabel!
    @IBOutlet weak var tripDestinationLabel: UILabel!
    @IBOutlet weak var tripSummaryDoneButton: UIButton!
    @IBOutlet weak var tripConversationFloatingActionButton: NativeRoundedButton!
    
    let locationManager = CLLocationManager()
    let mapPerspectiveInMeters: Double = 10000
    
    @IBOutlet weak var milestoneSummaryTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var milestoneSummaryTableView: UITableView!
    let milestoneSummaryTableViewCellHeight = 178
    let milestoneSummaryTableViewCellName: String = "MilestoneSummaryTableViewCell"
    
    @IBOutlet weak var recommendationCollectionView: UICollectionView!
    let recommendation: [String] = ["Riding Gear", "Winter Wear", "Drinking Water"]
    let ovalCollectionViewCellName: String = "OvalCollectionViewCell"
    
    @IBOutlet weak var riderCollectionView: UICollectionView!
    let riderCollectionViewCellName: String = "RiderCircleCollectionViewCell"
    
    // Runtime Attributes
    var tripSummaryType: TripSummaryType?
    
    // Segues
    let riderSelectorSegue = "TripSummaryToRiderSelector"
    let tripCreationSuccessSegue = "TripSummaryToTripCreationSuccess"
    let tripStartSegue = "TripSummaryToTripStart"
    
    // Model Objects
    var trip: Trip?
    
    deinit {
        print("[deinit] Trip Summary")
    }
    
}

// View Load
extension TripSummaryViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupNavigation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMap()
        setupScreen()
    }
    
    private func setupNavigation() {
        self.title = "Trip Summary"
        self.navigationController?.setNativeNavigationBar(backButton: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    private func setupMap() {
        checkLocationServices()
    }
    
    private func setupScreen() {
        tripInformationContainerView.addCornerRadius(10)
        tripInformationContainerView.addShadow(shadowType: .depth1)
        
        setupScreenValues()
        setupMilestones()
        setupRecommendation()
        setupRiders()
    }
    
    private func setupScreenValues() {
        self.tripNameLabel.text = trip?.name
        self.tripSourceLabel.text = trip?.source.city
        self.tripDestinationLabel.text = trip?.destination.city
        self.tripStartDateLabel.text = parseStartAndEndDate(startDate: trip!.startDate, endDate: trip!.endDate)
        self.tripStartTimeLabel.text = trip?.startTime
        
        if tripSummaryType == TripSummaryType.notCreated {
            tripSummaryDoneButton.setTitle("CREATE", for: .normal)
            tripConversationFloatingActionButton.isHidden = true
        } else if tripSummaryType == TripSummaryType.created {
            tripSummaryDoneButton.setTitle("GO", for: .normal)
            tripConversationFloatingActionButton.isHidden = false
        }
        
    }
    
    private func parseStartAndEndDate(startDate: String, endDate: String) -> String {
        return startDate
    }
}

// Map Specific Functionalities (Location)
extension TripSummaryViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: mapPerspectiveInMeters, longitudinalMeters: mapPerspectiveInMeters)
        tripLocationMapView.setRegion(region, animated: true)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
    private func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on.
        }
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            tripLocationMapView.showsUserLocation = true
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            break
        case .denied:
            // Show alert instructing them how to turn on permissions
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            // Show an alert letting them know what's up
            break
        case .authorizedAlways:
            break
        }
    }
    
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: mapPerspectiveInMeters, longitudinalMeters: mapPerspectiveInMeters)
            tripLocationMapView.setRegion(region, animated: true)
        }
    }
    
}

// Milestone Specific
extension TripSummaryViewController: UITableViewDelegate, UITableViewDataSource {
    
    private func setupMilestones() {
        let cellNib = UINib(nibName: milestoneSummaryTableViewCellName, bundle: nil)
        milestoneSummaryTableView.register(cellNib, forCellReuseIdentifier: milestoneSummaryTableViewCellName)
        
        milestoneSummaryTableView.delegate = self
        milestoneSummaryTableView.dataSource = self
        
        milestoneSummaryTableViewHeight.constant = CGFloat(milestoneSummaryTableViewCellHeight) * CGFloat(trip!.milestones.count)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let milestone = trip?.milestones {
            return milestone.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let milestone = trip!.milestones[indexPath.row]
        let cell = milestoneSummaryTableView.dequeueReusableCell(withIdentifier: milestoneSummaryTableViewCellName, for: indexPath) as! MilestoneSummaryTableViewCell
        cell.setupValues(milestone: milestone, index: indexPath.row)
        
        if indexPath.row == 0 {
            cell.topTimelineStrand.isHidden = true
        }
        
        if indexPath.row == trip!.milestones.count - 1 {
            cell.bottomTimelineStrand.isHidden = true
        }
        
        return cell
    }
    
}

// Generic Collection View Handler Extension
extension TripSummaryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == recommendationCollectionView {
            return getRecommendationCount()
        } else {
            return getRiderCount()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == recommendationCollectionView {
            return getRecommendationCellSize(indexPath: indexPath)
        } else {
            return getRiderCellSize(indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == recommendationCollectionView {
            return getRecommendationCell(collectionView, indexPath: indexPath)
        } else {
            return getRiderCell(collectionView, indexPath: indexPath)
        }
    }
}

// Recommendation Collection View Controller
extension TripSummaryViewController {
    
    private func setupRecommendation() {
        let nib = UINib(nibName: ovalCollectionViewCellName, bundle: nil)
        recommendationCollectionView.register(nib, forCellWithReuseIdentifier: ovalCollectionViewCellName)
        recommendationCollectionView.backgroundColor = .white
        recommendationCollectionView.delegate = self
        recommendationCollectionView.dataSource = self
    }
    
    private func getRecommendationCount() -> Int {
        return recommendation.count
    }
    
    private func getRecommendationCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let title = recommendation[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ovalCollectionViewCellName, for: indexPath) as! OvalCollectionViewCell
        cell.setupValues(title: title)
        return cell
    }
    
    private func getRecommendationCellSize(indexPath: IndexPath) -> CGSize {
        var cellSize = recommendation[indexPath.row].size(withAttributes: nil)
        cellSize.height = 30
        cellSize.width += 30
        return cellSize
    }
    
}

// Rider Collection View Specific
extension TripSummaryViewController {
    private func setupRiders() {
        let nib = UINib(nibName: riderCollectionViewCellName, bundle: nil)
        riderCollectionView.register(nib, forCellWithReuseIdentifier: riderCollectionViewCellName)
        riderCollectionView.backgroundColor = .white
        riderCollectionView.delegate = self
        riderCollectionView.dataSource = self
    }
    
    private func getRiderCount() -> Int {
        return trip!.invitedRiders.count
    }
    
    private func getRiderCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: riderCollectionViewCellName, for: indexPath) as! RiderCircleCollectionViewCell
        cell.setDismissableState(state: false)
        return cell
    }
    
    private func getRiderCellSize(indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    
    func riderSelected(riders: [Rider]) {
        trip!.invitedRiders = riders
        riderCollectionView.reloadData()
    }
}

// On Data Entry Completion
extension TripSummaryViewController: SuccessScreenDelegate  {
    
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        if tripSummaryType == TripSummaryType.notCreated {
            performSegue(withIdentifier: tripCreationSuccessSegue, sender: self)
        } else if tripSummaryType == TripSummaryType.created {
            performSegue(withIdentifier: tripStartSegue, sender: self)
        }
    }
    
    // Delegate Method (SuccessScreeenDelegate)
    func doneButtonPressed() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

// Segue Specific
extension TripSummaryViewController: RiderSelectorDelegate {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == riderSelectorSegue {
            let invitedRiders = trip!.invitedRiders
            if let destination : RiderSelectorViewController = segue.destination as? RiderSelectorViewController {
                destination.selectedRiders = invitedRiders
                destination.delegate = self
            }
        }
        
        if segue.identifier == tripCreationSuccessSegue {
            if let destination : SuccessScreenViewController = segue.destination as? SuccessScreenViewController {
                destination.successMessageTitle = "Success!!"
                destination.successMessageDescription = "The Trip has been successfully created!"
                destination.delegate = self
            }
        }
        
    }
    
}
