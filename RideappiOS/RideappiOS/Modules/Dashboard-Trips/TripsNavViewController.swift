//
//  TripsNavViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 04/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class TripsNavViewController: UINavigationController {
    
    let trips = [
        Trip(
            image: #imageLiteral(resourceName: "manali"), name: "Manali", startDate: "1/2/2019", startTime: "10:30", endDate: "2/2/2019",
            source: Place(name: "Goa", city: "Goa", pinCode: 574110, distance: 300, latlong: [10,11]),
            destination: Place(name: "Hassan", city: "Hassan", pinCode: 504110, distance: 400, latlong: [100,200]),
            invitedRiders: [],
            milestones: []
        )
    ]
    
    let tripExistsSegue: String = "TripExists"
    let tripNotExistsSegue: String = "TripNotExists"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setRootViewController()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

    private func setupNavigation() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    private func setRootViewController() {
        if isTripExists() {
            performSegue(withIdentifier: tripExistsSegue, sender: self)
        } else {
            performSegue(withIdentifier: tripNotExistsSegue, sender: self)
        }
    }
    
    private func isTripExists() -> Bool {
        if trips.count > 0 {
            return true
        } else {
            return false
        }
    }
    
    deinit {
        print("[deinit] Trips Nav View")
    }
    
}

// Segue Specific
extension TripsNavViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == tripExistsSegue {
            if let destination : TripsViewController = segue.destination as? TripsViewController {
                destination.trips = trips
            }
        }
    }
    
    @IBAction func unwindToTripNav(_ sender: UIStoryboardSegue) {}
    
}
