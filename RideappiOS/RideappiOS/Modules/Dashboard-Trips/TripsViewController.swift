//
//  TripsViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 03/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class TripsViewController: UIViewController {
    
    @IBOutlet weak var tripThumbnailTableView: UITableView!
    
    let cellSpacingHeight : CGFloat = 15
    let tabBarItemTitle = "Trips"
    var trips : [Trip] = [] // This variable is populated by the parent via segue
    var selectedTrip: Trip?
    
    // Segues
    let tripSummarySegue: String = "TripsToTripSummary"
    
    deinit {
        print("Trips View Deinitialized")
    }
    
}

// View Loading Segment
extension TripsViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    private func setupComponents() {
        setupTrips()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupNavigation()
    }
    
    private func setupNavigation() {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
}

// Trips Table View Segment
extension TripsViewController : UITableViewDataSource, UITableViewDelegate {
    
    private func setupTrips() {
        if trips.count > 0 { setupTripThumbnailTableView() }
    }
    
    private func setupTripThumbnailTableView() {
        let nib = UINib(nibName: "TripThumbnailTableViewCell", bundle: nil)
        tripThumbnailTableView.register(nib, forCellReuseIdentifier: "TripThumbnailTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let trip = trips[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripThumbnailTableViewCell") as! TripThumbnailTableViewCell
        cell.setupValues(trip: trip)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let trip = trips[indexPath.row]
        selectedTrip = trip
        performSegue(withIdentifier: tripSummarySegue, sender: self)
    }
    
}

// Segue Specific
extension TripsViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == tripSummarySegue {
            if let destination: TripSummaryViewController = segue.destination as? TripSummaryViewController {
                destination.tripSummaryType = .created
                destination.trip = selectedTrip
            }
        }
    }
    
}
