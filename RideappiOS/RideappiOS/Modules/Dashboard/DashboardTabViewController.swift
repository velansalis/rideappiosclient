//
//  DashboardTabViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 03/07/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class DashboardTabViewController: UITabBarController {

    let tabBarItems = ["Trips", "Activities", "Garage", "Profile", "More"]
    var selectedTabItem: UITabBarItem?
    let selectedItemColor: UIColor = UIColor.getColor(.PrimaryWhite)
    let unselectedItemColor: UIColor = UIColor.getColor(.SecondaryWhite)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let colors = [
            UIColor.getColor(.PrimaryOrange).cgColor,
            UIColor.getColor(.SecondaryOrange).cgColor
        ]
        
        self.tabBar.tintColor = selectedItemColor
        self.tabBar.unselectedItemTintColor = unselectedItemColor
        let gradientLayer = UIColor.getGradientLayer(colors, bounds: self.tabBar.frame)
        self.tabBar.backgroundImage = UIImage.image(fromLayer: gradientLayer)
        self.tabBar.addShadow(shadowType: .depth3)
        self.tabBar.items![0].title = "Hello"
    }

    deinit {
        print("[deinit] Dashboard tab")
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        selectedTabItem?.title = ""
        selectedTabItem = item
        selectedTabItem?.title = tabBarItems[item.tag - 1]
    }
    
}
