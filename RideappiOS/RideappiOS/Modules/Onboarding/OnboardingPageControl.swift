//
//  OnboardingPageControl.swift
//  RideappiOS
//
//  Created by Velan Salis on 29/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class OnboardingPageControl: UIPageControl {

    override init(frame: CGRect) {
        super.init(frame : frame)
        self.setupPageControl()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
    }
    
    private func setupPageControl() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.currentPageIndicatorTintColor = UIColor.orange
        self.pageIndicatorTintColor = .gray
    }

}
