//
//  OnboardingViewController.swift
//  RideappiOS
//
//  Created by Velan Salis on 29/06/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

class OnboardingViewController: UIPageViewController, UIPageViewControllerDataSource , UIPageViewControllerDelegate{
    
    private var pageControl = OnboardingPageControl(frame: .zero)
    
    lazy var appIntroViewControllerList : [UIViewController] = {
        let storyBoard = UIStoryboard(name: "Onboarding", bundle: nil)
        let OnboardingScreen1 = storyBoard.instantiateViewController(withIdentifier: "Onboarding-Screen1")
        let OnboardingScreen2 = storyBoard.instantiateViewController(withIdentifier: "Onboarding-Screen2")
        let OnboardingScreen3 = storyBoard.instantiateViewController(withIdentifier: "Onboarding-Screen3")
        return [OnboardingScreen1, OnboardingScreen2, OnboardingScreen3]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        self.setupPageControl()
        self.setupNavigationControl()
        if let firstView = appIntroViewControllerList.first {
            self.setViewControllers([firstView], direction: .forward, animated: true, completion: nil)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = appIntroViewControllerList.firstIndex(of: pageContentViewController)!
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentViewControllerIndex : Int = appIntroViewControllerList.firstIndex(of: viewController) else { return nil }
        let nextViewControllerIndex : Int = currentViewControllerIndex + 1
        guard appIntroViewControllerList.count != nextViewControllerIndex else { return nil }
        guard appIntroViewControllerList.count > nextViewControllerIndex else { return nil }
        return appIntroViewControllerList[nextViewControllerIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentViewControllerIndex : Int = appIntroViewControllerList.firstIndex(of: viewController) else { return nil }
        let previousViewControllerIndex : Int = currentViewControllerIndex - 1
        guard previousViewControllerIndex >= 0 else { return nil }
        guard appIntroViewControllerList.count > previousViewControllerIndex else { return nil }
        return appIntroViewControllerList[previousViewControllerIndex]
    }
    
    private func setupPageControl() {
        pageControl.numberOfPages = appIntroViewControllerList.count
        let leading = NSLayoutConstraint(item: pageControl, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: pageControl, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: pageControl, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -20)
        view.insertSubview(pageControl, at: 0)
        view.bringSubviewToFront(pageControl)
        view.addConstraints([leading, trailing, bottom])
    }
    
    private func setupNavigationControl() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.view.tintColor = .orange
        let uiBarButton = UIBarButtonItem(title: "Skip", style: .done, target: self, action: #selector(skipButtonTapped))
        self.navigationItem.rightBarButtonItem = uiBarButton
    }
    
    @objc func skipButtonTapped() {
        performSegue(withIdentifier: "skipToAuthentication", sender: self)
    }
    
}
