#  RideApp ( iOS )
## Description
RideApp is an assistant app for riders who want to go on big adventures on their bike but not finding their pack. Well, this app is designed for connecting riders to each other in certain areas and enabling them to go on rides together. By using rideapp you can find your type of ride in your comfortable time and make sure you don't miss out on any of your rides. Because You only live once, right? :) 

## Branches
- master
- develop (development branch)
- feature/* (feature branch)

## Modules (Progress)
### Authentication Module
- [] UI *( Currently working : Maps )* 
- [ ] Networking Implementation
- [ ] API Integration 
- [ ] Core data Implementation
- [ ] Memory leak refactoring

### Trips
- [ ] UI 
- [ ] Networking Implementation
- [ ] API Integration 
- [ ] Core data Implementation
- [ ] Memory leak refactoring

### Garage
- [ ] UI 
- [ ] Networking Implementation
- [ ] API Integration 
- [ ] Core data Implementation
- [ ] Memory leak refactoring

### Activities
- [ ] UI 
- [ ] Networking Implementation
- [ ] API Integration 
- [ ] Core data Implementation
- [ ] Memory leak refactoring

### Profile
- [ ] UI 
- [ ] Networking Implementation
- [ ] API Integration 
- [ ] Core data Implementation
- [ ] Memory leak refactoring

### More
- [ ] UI 
- [ ] Networking Implementation
- [ ] API Integration 
- [ ] Core data Implementation
- [ ] Memory leak refactoring

## Note to self (Learning from mistakes)
### Project Organization
- Trace down all the **common elements** in the project. Be it storyboards or common views. And put it in their separate entities. If the entities are dependent on certain values populate them via segue. For example, The responsibility of a maps scene is to show the map to the user. Maps scene is common to all scenes and it is reusable. Maps scene depends on source, destination and milestone variables to be populated before loading. So, whenever you want to use maps, populate those variables via segue by keeping it totally independent from other scenens. (Example in RideappiOS/CommonUI/Storyboard/MapViewController)
- **Organize the project modules based on their functionality** and organize other targets based on their types. For example, All the modules are organized under Modules group /Extensions under extensions / Common UI under group common UI. And all the Modules are organized based on their functionality (RideappiOS/Modules/). This makes working easier.
- Use **Storyboard References:** to organize the project based on their workflow. For example, all the modules will have their own workflow. So they get their own storyboard inside that module. If one and only one person is working on that storyboard, it is easier when there is a need to merge. Since only one person worked on that one storyboard, there won't be any merge conflict.
- Try to make the scenes and components as independent as possible so that you can just populate one variable and it will just work. Which will help you in reusing the component without much hassle.  

### Data Passing
- Use delegates and segues to pass data between view controllers. DO NOT USE STATIC, GLOBAL OR SINGLETON.  If you use static, the memory won't be released and it could cause retain cycles. Instead of static and singleton classes, use extensions to the native UI elements to extend their functionality (Example in RideappiOS/Extensions/)

### Source Control
- DO NOT WORK DIRECTLY ON MASTER!! Create a separate branch like 'develop'. And when a feature needs to be added, create a branch from develop, called feature/<your branch name>. Once the implementation is done, merge your branch with develop. When the module is ready to be produced, merge develop with master. (This is one among so many workflows that are under use. This was easy and worked for me) 

Have a nice day :)
